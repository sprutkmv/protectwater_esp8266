//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "driver/adc.h"

#include "light_sensor.h"
#include "device_mqtt.h"
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "light_sensor";

//void (*light_sensor_evt)(int) = NULL;
//-----------------------------------------------------------------------------------------------
static void light_sensor_task();

//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
void light_sensor_init() {
	// 1. init adc
	adc_config_t adc_config;
	// Depend on menuconfig->Component config->PHY->vdd33_const value
	// When measuring system voltage(ADC_READ_VDD_MODE), vdd33_const must be set to 255.
	adc_config.mode = ADC_READ_TOUT_MODE;
	adc_config.clk_div = 8; // ADC sample collection clock = 80MHz/clk_div = 10MHz
	ESP_ERROR_CHECK(adc_init(&adc_config));

	// 2. Create a adc task to read adc value
	xTaskCreate(light_sensor_task, "light_sensor_task", 1024, NULL, 2, NULL);
}
//-----------------------------------------------------------------------------------------------
//void light_sensor_set_evt(void (*evt)(int)) {
//	light_sensor_evt = evt;
//}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void light_sensor_task() {
	uint16_t adc_data;
	mqtt_packet_t mqtt_packet;
	while (1) {
		if (ESP_OK == adc_read(&adc_data)) {
			ESP_LOGI(TAG, "adc read: %d", adc_data);
			strcpy(mqtt_packet.topic, "/pw/light_sensor");
			sprintf(mqtt_packet.data, "%d", adc_data);
			device_mqtt_published(&mqtt_packet);
//			if (light_sensor_evt != NULL) {
//				light_sensor_evt(adc_data[0]);
//			}
		}
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}
//-----------------------------------------------------------------------------------------------
