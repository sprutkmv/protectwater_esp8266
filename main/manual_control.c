//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "esp_system.h"

#include "manual_control.h"
#include "keyboard.h"
#include "protect_water.h"
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "manual_control";
//static xQueueHandle manual_control_evt_queue = NULL;
//-----------------------------------------------------------------------------------------------
void manual_control_init();
void manual_control_init_gpio();
static void manual_control_task(void *arg);
static void manual_control_error_led_task(void *arg);
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
void manual_control_init() {
	InitKeyBoard();
	manual_control_init_gpio();
	xTaskCreate(manual_control_task, "manual_control_task", 1024, NULL, 2, NULL);
	xTaskCreate(manual_control_error_led_task, "manual_control_error_led_task", 1024, NULL, 2, NULL);
}

//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void manual_control_task(void *arg) {

	TStateButton	key_button1;
	while (1) {
		ScanKeyBoard();
		key_button1= GetStateButton(BUTTON_1);
		if (key_button1 == KEY_UP) {
			ESP_LOGI(TAG, "BUTTON_1: KEY_UP");
			protect_water_tap_close();
		} else if (key_button1 == KEY_DELAY) {
			ESP_LOGI(TAG, "BUTTON_1: KEY_DELAY");
			protect_water_tap_open();
		}
		vTaskDelay(10 / portTICK_RATE_MS);
	}
}
//-----------------------------------------------------------------------------------------------
static void manual_control_error_led_task(void *arg) {
	while (1) {
//		ESP_LOGI(TAG, "error_led: %d\n", i++);
		gpio_set_level(MC_GPIO_OUTPUT_BUILTIN_LED, 0);
		vTaskDelay(100 / portTICK_RATE_MS);
		gpio_set_level(MC_GPIO_OUTPUT_BUILTIN_LED, 1);
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}
//-----------------------------------------------------------------------------------------------
void manual_control_init_gpio() {

	gpio_config_t io_conf;
//	io_conf.intr_type = GPIO_INTR_NEGEDGE;		//interrupt of rising edge
//	io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;	//bit mask of the pins, use GPIO4/5 here
//	io_conf.mode = GPIO_MODE_INPUT;				//set as input mode
//	io_conf.pull_up_en = 1;						//enable pull-up mode
//	gpio_config(&io_conf);
//	//change gpio intrrupt type for one pin
//	gpio_set_intr_type(GPIO_INPUT_BUTTON, GPIO_INTR_NEGEDGE);

	io_conf.intr_type = GPIO_INTR_DISABLE;	//disable interrupt
	io_conf.mode = GPIO_MODE_OUTPUT;		//set as output mode
	io_conf.pin_bit_mask = MC_GPIO_OUTPUT_PIN_SEL;		//bit mask of the pins that you want to set,
	io_conf.pull_down_en = 0;	//disable pull-down mode
	io_conf.pull_up_en = 0;		//disable pull-up mode
	gpio_config(&io_conf);		//configure GPIO with the given settings
}
//-----------------------------------------------------------------------------------------------

