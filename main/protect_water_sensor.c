//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_log.h"

#include "protect_water_sensor.h"
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "protect_water_sensor";
static sensor_t sensors[PWS_MAX_COUNT_SENSORS];
xSemaphoreHandle pwsMutex;

//static xQueueHandle protect_water_sensor_evt_queue = NULL;
void (*protect_water_sensor_evt)(int) = NULL;
//-----------------------------------------------------------------------------------------------
void protect_water_sensor_init_gpio();
static void protect_water_sensor_task(void *arg);
void protect_water_sensor_set_state(uint32_t num, state_sensor_t state);
//state_sensor_t protect_water_sensor_get_state(uint32_t num);
void protect_water_sensor_reset_state(uint32_t num);
void protect_water_sensor_scan();
uint8_t protect_water_sensor_get_state_pin(pin_sensor_t *pin_sensor);
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
void protect_water_sensor_init() {

	sensors[0].pin.GPIO_Pin = PWS_GPIO_INPUT_PWS_SENSOR_1;
	sensors[0].pin.max_count_dreb = PWS_MAX_COUNT_DREB;
	sensors[0].state = STATE_SENSOR_NORMAL;

	sensors[1].pin.GPIO_Pin = PWS_GPIO_INPUT_PWS_SENSOR_2;
	sensors[1].pin.max_count_dreb = PWS_MAX_COUNT_DREB;
	sensors[1].state = STATE_SENSOR_NORMAL;

	sensors[2].pin.GPIO_Pin = PWS_GPIO_INPUT_PWS_SENSOR_3;
	sensors[2].pin.max_count_dreb = PWS_MAX_COUNT_DREB;
	sensors[2].state = STATE_SENSOR_NORMAL;

	protect_water_sensor_init_gpio();
	pwsMutex = xSemaphoreCreateMutex();
	xTaskCreate(protect_water_sensor_task, "protect_water_sensor_task", 1024, NULL, 2, NULL);
}
//-----------------------------------------------------------------------------------------------
uint32_t protect_water_sensor_max_num() {
	return (PWS_MAX_COUNT_SENSORS);
}
//-----------------------------------------------------------------------------------------------
state_sensor_t protect_water_sensor_get_state(uint32_t num) {
	state_sensor_t state = STATE_SENSOR_ERROR;
//	if ((num < PWS_MAX_COUNT_SENSORS) & (num >= 0)) {
	if (num < PWS_MAX_COUNT_SENSORS) {
		xSemaphoreTake(pwsMutex, portMAX_DELAY);
		{
			state = sensors[num].state;
		}
		xSemaphoreGive(pwsMutex);
	}
	return (state);
}
//-----------------------------------------------------------------------------------------------
void protect_water_sensor_set_evt(void (*evt)(int)) {
	protect_water_sensor_evt = evt;
}
//-----------------------------------------------------------------------------------------------
//void protect_water_sensor_set_evt(xQueueHandle evt_queue) {
//	protect_water_sensor_evt_queue = evt_queue;
//}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void protect_water_sensor_task(void *arg) {

	while (1) {
		protect_water_sensor_scan();
		for (int i = 0; i < PWS_MAX_COUNT_SENSORS; i++) {
			state_sensor_t state = protect_water_sensor_get_state(i);
			if (state == STATE_SENSOR_ALARM) {
				protect_water_sensor_reset_state(i);
				ESP_LOGD(TAG, "sensor[%d], state: %d", i, state);
				if (protect_water_sensor_evt != NULL) {
//					xQueueSend(protect_water_sensor_evt_queue, &i, PWS_LOOP_MS);
					protect_water_sensor_evt(i);
				}
			}
		}
		vTaskDelay(PWS_LOOP_MS / portTICK_RATE_MS);
	}
}
//-----------------------------------------------------------------------------------------------
void protect_water_sensor_set_state(uint32_t num, state_sensor_t state) {
	if (num < PWS_MAX_COUNT_SENSORS) {
		xSemaphoreTake(pwsMutex, portMAX_DELAY);
		{
			sensors[num].state = state;
		}
		xSemaphoreGive(pwsMutex);
	}
}
//-----------------------------------------------------------------------------------------------
void protect_water_sensor_init_gpio() {

	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE;	//disable interrupt
//	io_conf.intr_type = GPIO_INTR_NEGEDGE;		//interrupt of rising edge
	io_conf.pin_bit_mask = PWS_GPIO_INPUT_PIN_SEL;	//bit mask of the pins, use GPIO4/5 here
	io_conf.mode = GPIO_MODE_INPUT;		//set as input mode
	io_conf.pull_up_en = 1;				//enable pull-up mode
	gpio_config(&io_conf);

	//change gpio intrrupt type for one pin
//	gpio_set_intr_type(PWS_GPIO_INPUT_PWS_SENSOR_1, GPIO_INTR_NEGEDGE);
//	gpio_set_intr_type(PWS_GPIO_INPUT_PWS_SENSOR_2, GPIO_INTR_NEGEDGE);
}
//-----------------------------------------------------------------------------------------------
void protect_water_sensor_scan() {
	for (int i = 0; i < PWS_MAX_COUNT_SENSORS; i++) {
		if (protect_water_sensor_get_state_pin(&(sensors[i].pin))) {
			protect_water_sensor_set_state(i, STATE_SENSOR_ALARM);
			ESP_LOGV(TAG, "sensor[%d], state: STATE_SENSOR_ALARM", i);
		} else {
			protect_water_sensor_set_state(i, STATE_SENSOR_NORMAL);
			ESP_LOGV(TAG, "sensor[%d], state: STATE_SENSOR_NORMAL", i);
		}
	}
}
//-----------------------------------------------------------------------------------------------
uint8_t protect_water_sensor_get_state_pin(pin_sensor_t *pin_sensor) {
	// ������ �� ������ 1
	if (!gpio_get_level(pin_sensor->GPIO_Pin)) {
		//��, ��������� �� ����� ���������� ��������?
		if (pin_sensor->count_dreb > pin_sensor->max_count_dreb) {
			pin_sensor->key_press = 1;        // ���������� ���� ���������
		} else {
			pin_sensor->count_dreb++;      //�� ��������,  ���������� ����������
		}
	} else {
		pin_sensor->key_press = 0;  // �������� ���� ���������
		pin_sensor->count_dreb = 0; // �������� ������� ���������� ���������� ��������
	}
	return (pin_sensor->key_press);
}
//-----------------------------------------------------------------------------------------------
void protect_water_sensor_reset_state(uint32_t num) {
	if (num < PWS_MAX_COUNT_SENSORS) {
		sensors[num].state = STATE_SENSOR_NORMAL;
		sensors[num].pin.key_press = 0;
		sensors[num].pin.count_dreb = 0;
	}
}
//-----------------------------------------------------------------------------------------------
