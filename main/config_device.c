//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "esp_system.h"

#include "esp_spiffs.h"
#include "esp_vfs.h"
#include "cJSON.h"

#include "config_device.h"

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "config_device";
static config_device_t configuration_device;
char json_buff[1024];
//static char filepath[64];
static char c_base_path[ESP_VFS_PATH_MAX + 1];
//-----------------------------------------------------------------------------------------------
esp_err_t config_device_set_default(config_device_t *config_device);
esp_err_t config_mqtt_set_default(config_mqtt_t *config_mqtt);
esp_err_t config_wifi_set_default(config_wifi_t *config_wifi);
esp_err_t config_sys_set_default(config_sys_t *config_sys);

//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
esp_err_t config_device_init(const char *base_path) {
	ESP_LOGI(TAG, "Initializing Config File");
	strlcpy(c_base_path, base_path, sizeof(c_base_path));
	config_device_load_file(&configuration_device);
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
config_device_t* config_device_get_ptr() {
	return (&configuration_device);
}
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
esp_err_t config_wifi_create_json_str(config_wifi_t *config_wifi, char *jbuff, uint32_t size) {

	cJSON *jconfig_wifi = NULL;
	jconfig_wifi = cJSON_CreateObject();
	if (jconfig_wifi == NULL) {
		ESP_LOGE(TAG, "Creat_json_wifi_str, Error");
		return (ESP_FAIL);
	}
	cJSON_AddStringToObject(jconfig_wifi, "ssid", config_wifi->ssid);
	cJSON_AddStringToObject(jconfig_wifi, "password", config_wifi->password);
	cJSON_AddStringToObject(jconfig_wifi, "ssid_ap", config_wifi->ssid_ap);
	cJSON_AddStringToObject(jconfig_wifi, "password_ap", config_wifi->password_ap);
	cJSON_AddNumberToObject(jconfig_wifi, "max_connection", config_wifi->max_connection);
	cJSON_AddNumberToObject(jconfig_wifi, "authmode", config_wifi->authmode);
	cJSON_AddNumberToObject(jconfig_wifi, "wifi_mode", config_wifi->wifi_mode);

	cJSON_PrintPreallocated(jconfig_wifi, jbuff, size, 1);
	cJSON_Delete(jconfig_wifi);

	ESP_LOGD(TAG, "Creat_json_wifi_str: %s", jbuff);
	ESP_LOGI(TAG, "Creat_json_wifi_str, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_sys_create_json_str(config_sys_t *config_sys, char *jbuff, uint32_t size) {

	cJSON *jconfig_sys = NULL;
	jconfig_sys = cJSON_CreateObject();
	if (jconfig_sys == NULL) {
		ESP_LOGE(TAG, "Creat_json_sys_str, Error");
		return (ESP_FAIL);
	}

	cJSON_AddStringToObject(jconfig_sys, "host_name", config_sys->host_name);
	cJSON_AddStringToObject(jconfig_sys, "timezone", config_sys->timezone);
	cJSON_AddStringToObject(jconfig_sys, "sntp_server", config_sys->sntp_server);

	cJSON_PrintPreallocated(jconfig_sys, jbuff, size, 1);
	cJSON_Delete(jconfig_sys);

	ESP_LOGD(TAG, "Creat_json_sys_str: %s", jbuff);
	ESP_LOGI(TAG, "Creat_json_sys_str, Ok");
	return (ESP_OK);

}
//-----------------------------------------------------------------------------------------------
esp_err_t config_mqtt_create_json_str(config_mqtt_t *config_mqtt, char *jbuff, uint32_t size) {

	cJSON *jconfig_mqtt = NULL;
	jconfig_mqtt = cJSON_CreateObject();
	if (jconfig_mqtt == NULL) {
		ESP_LOGE(TAG, "Creat_json_mqtt_str, Error");
		return (ESP_FAIL);
	}

	cJSON_AddStringToObject(jconfig_mqtt, "uri", config_mqtt->uri);
	cJSON_AddNumberToObject(jconfig_mqtt, "port", config_mqtt->port);
	cJSON_AddStringToObject(jconfig_mqtt, "username", config_mqtt->username);
	cJSON_AddStringToObject(jconfig_mqtt, "password", config_mqtt->password);

	cJSON_PrintPreallocated(jconfig_mqtt, jbuff, size, 1);
	cJSON_Delete(jconfig_mqtt);

	ESP_LOGD(TAG, "Creat_json_mqtt_str: %s", jbuff);
	ESP_LOGI(TAG, "Creat_json_mqtt_str, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
esp_err_t config_wifi_parser_json_str(config_wifi_t *config_wifi, char *jbuff) {

	cJSON *jconfig_wifi = NULL;
	jconfig_wifi = cJSON_Parse(jbuff);
	if (jconfig_wifi == NULL) {
		ESP_LOGE(TAG, "Parser_json_wifi_str, Error");
		return (ESP_FAIL);
	}

	cJSON *ssid = cJSON_GetObjectItemCaseSensitive(jconfig_wifi, "ssid");
	if (cJSON_IsString(ssid) && (ssid->valuestring != NULL)) {
		strlcpy(config_wifi->ssid, ssid->valuestring, sizeof(config_wifi->ssid));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, ssid");
		return (ESP_FAIL);
	}
	cJSON *password = cJSON_GetObjectItemCaseSensitive(jconfig_wifi, "password");
	if (cJSON_IsString(password) && (password->valuestring != NULL)) {
		strlcpy(config_wifi->password, password->valuestring, sizeof(config_wifi->password));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, password");
		return (ESP_FAIL);
	}
	cJSON *ssid_ap = cJSON_GetObjectItemCaseSensitive(jconfig_wifi, "ssid_ap");
	if (cJSON_IsString(ssid_ap) && (ssid_ap->valuestring != NULL)) {
		strlcpy(config_wifi->ssid_ap, ssid_ap->valuestring, sizeof(config_wifi->ssid_ap));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, ssid_ap");
		return (ESP_FAIL);
	}
	cJSON *password_ap = cJSON_GetObjectItemCaseSensitive(jconfig_wifi, "password_ap");
	if (cJSON_IsString(password_ap) && (password_ap->valuestring != NULL)) {
		strlcpy(config_wifi->password_ap, password_ap->valuestring, sizeof(config_wifi->password_ap));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, password_ap");
		return (ESP_FAIL);
	}
	cJSON *max_connection = cJSON_GetObjectItemCaseSensitive(jconfig_wifi, "max_connection");
	if (cJSON_IsNumber(max_connection)) {
		config_wifi->max_connection = max_connection->valueint;
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, max_connection");
		return (ESP_FAIL);
	}
	cJSON *authmode = cJSON_GetObjectItemCaseSensitive(jconfig_wifi, "authmode");
	if (cJSON_IsNumber(authmode)) {
		config_wifi->authmode = authmode->valueint;
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, authmode");
		return (ESP_FAIL);
	}
	cJSON *wifi_mode = cJSON_GetObjectItemCaseSensitive(jconfig_wifi, "wifi_mode");
	if (cJSON_IsNumber(wifi_mode)) {
		config_wifi->wifi_mode = wifi_mode->valueint;
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, wifi_mode");
		return (ESP_FAIL);
	}

	ESP_LOGD(TAG, "config_wifi->ssid: %s", config_wifi->ssid);
	ESP_LOGD(TAG, "config_wifi->password: %s", config_wifi->password);
	ESP_LOGD(TAG, "config_wifi->ssid_ap: %s", config_wifi->ssid_ap);
	ESP_LOGD(TAG, "config_wifi->password_ap: %s", config_wifi->password_ap);
	ESP_LOGD(TAG, "config_wifi->max_connection: %d", config_wifi->max_connection);
	ESP_LOGD(TAG, "config_wifi->authmode: %d", config_wifi->authmode);
	ESP_LOGD(TAG, "config_wifi->wifi_mode: %d", config_wifi->wifi_mode);

	ESP_LOGI(TAG, "Parser_json_wifi_str, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_sys_parser_json_str(config_sys_t *config_sys, char *jbuff) {

	cJSON *jconfig_sys = NULL;
	jconfig_sys = cJSON_Parse(jbuff);
	if (jconfig_sys == NULL) {
		ESP_LOGE(TAG, "Parser_json_sys_str, Error");
		return (ESP_FAIL);
	}

	cJSON *host_name = cJSON_GetObjectItemCaseSensitive(jconfig_sys, "host_name");
	if (cJSON_IsString(host_name) && (host_name->valuestring != NULL)) {
		strlcpy(config_sys->host_name, host_name->valuestring, sizeof(config_sys->host_name));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, host_name");
		return (ESP_FAIL);
	}
	cJSON *timezone = cJSON_GetObjectItemCaseSensitive(jconfig_sys, "timezone");
	if (cJSON_IsString(timezone) && (timezone->valuestring != NULL)) {
		strlcpy(config_sys->timezone, timezone->valuestring, sizeof(config_sys->timezone));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, timezone");
		return (ESP_FAIL);
	}
	cJSON *sntp_server = cJSON_GetObjectItemCaseSensitive(jconfig_sys, "sntp_server");
	if (cJSON_IsString(sntp_server) && (sntp_server->valuestring != NULL)) {
		strlcpy(config_sys->sntp_server, sntp_server->valuestring, sizeof(config_sys->sntp_server));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, sntp_server");
		return (ESP_FAIL);
	}
	cJSON_Delete(jconfig_sys);

	ESP_LOGD(TAG, "config_sys->host_name: %s", config_sys->host_name);
	ESP_LOGD(TAG, "config_sys->timezone: %s", config_sys->timezone);
	ESP_LOGD(TAG, "config_sys->sntp_server: %s", config_sys->sntp_server);

	ESP_LOGI(TAG, "Parser_json_sys_str, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_mqtt_parser_json_str(config_mqtt_t *config_mqtt, char *jbuff) {

	cJSON *jconfig_mqtt = NULL;
	jconfig_mqtt = cJSON_Parse(jbuff);
	if (jconfig_mqtt == NULL) {
		ESP_LOGE(TAG, "Parser_json_mqtt_str, Error");
		return (ESP_FAIL);
	}

	cJSON *uri = cJSON_GetObjectItemCaseSensitive(jconfig_mqtt, "uri");
	if (cJSON_IsString(uri) && (uri->valuestring != NULL)) {
		strlcpy(config_mqtt->uri, uri->valuestring, sizeof(config_mqtt->uri));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, uri");
		return (ESP_FAIL);
	}
	cJSON *password = cJSON_GetObjectItemCaseSensitive(jconfig_mqtt, "password");
	if (cJSON_IsString(password) && (password->valuestring != NULL)) {
		strlcpy(config_mqtt->password, password->valuestring, sizeof(config_mqtt->password));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, password");
		return (ESP_FAIL);
	}
	cJSON *username = cJSON_GetObjectItemCaseSensitive(jconfig_mqtt, "username");
	if (cJSON_IsString(username) && (username->valuestring != NULL)) {
		strlcpy(config_mqtt->username, username->valuestring, sizeof(config_mqtt->username));
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, username");
		return (ESP_FAIL);
	}
	cJSON *port = cJSON_GetObjectItemCaseSensitive(jconfig_mqtt, "port");
	if (cJSON_IsNumber(port)) {
		config_mqtt->port = port->valueint;
	} else {
		ESP_LOGE(TAG, "Failed cJSON_Get, port");
		return (ESP_FAIL);
	}

	cJSON_Delete(jconfig_mqtt);

	ESP_LOGD(TAG, "config_mqtt->uri: %s", config_mqtt->uri);
	ESP_LOGD(TAG, "config_mqtt->password: %s", config_mqtt->password);
	ESP_LOGD(TAG, "config_mqtt->username: %s", config_mqtt->username);
	ESP_LOGD(TAG, "config_mqtt->port: %d", config_mqtt->port);

	ESP_LOGI(TAG, "Parser_json_mqtt_str, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_device_save_file(config_device_t *config_device) {
	config_wifi_save_file(&config_device->wifi);
	config_sys_save_file(&config_device->sys);
	config_mqtt_save_file(&config_device->mqtt);
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_wifi_save_file(config_wifi_t *config_wifi) {

	FILE *fd = NULL;
	ssize_t write_bytes;
	static char filepath[64];

	snprintf(filepath, sizeof(filepath), "%s/%s", c_base_path, CONFIG_ESP_FILE_CONFIG_WIFI);

	if (config_wifi_create_json_str(config_wifi, json_buff, sizeof(json_buff)) != ESP_OK) {
		return (ESP_FAIL);
	}

	fd = fopen(filepath, "w");
	if (fd == NULL) {
		ESP_LOGI(TAG, "Save_file, Error");
		return (ESP_FAIL);
	} else {

		write_bytes = fprintf(fd, "%s", json_buff);
		if (write_bytes != strlen(json_buff)) {
			ESP_LOGE(TAG, "Failed to write file (w): %s, bytes: %d", filepath, write_bytes);
			return (ESP_FAIL);
		}
		ESP_LOGD(TAG, "Write file %s, bytes: %d", filepath, write_bytes);
		fclose(fd);
	}
	ESP_LOGI(TAG, "Save_file, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_sys_save_file(config_sys_t *config_sys) {

	FILE *fd = NULL;
	ssize_t write_bytes;
	static char filepath[64];

	snprintf(filepath, sizeof(filepath), "%s/%s", c_base_path, CONFIG_ESP_FILE_CONFIG_SYS);

	if (config_sys_create_json_str(config_sys, json_buff, sizeof(json_buff)) != ESP_OK) {
		return (ESP_FAIL);
	}

	fd = fopen(filepath, "w");
	if (fd == NULL) {
		ESP_LOGI(TAG, "Save_file, Error");
		return (ESP_FAIL);
	} else {

		write_bytes = fprintf(fd, "%s", json_buff);
		if (write_bytes != strlen(json_buff)) {
			ESP_LOGE(TAG, "Failed to write file (w): %s, bytes: %d", filepath, write_bytes);
			return (ESP_FAIL);
		}
		ESP_LOGD(TAG, "Write file %s, bytes: %d", filepath, write_bytes);
		fclose(fd);
	}
	ESP_LOGI(TAG, "Save_file, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_mqtt_save_file(config_mqtt_t *config_mqtt) {

	FILE *fd = NULL;
	ssize_t write_bytes;
	static char filepath[64];

	snprintf(filepath, sizeof(filepath), "%s/%s", c_base_path, CONFIG_ESP_FILE_CONFIG_MQTT);

	if (config_mqtt_create_json_str(config_mqtt, json_buff, sizeof(json_buff)) != ESP_OK) {
		return (ESP_FAIL);
	}

	fd = fopen(filepath, "w");
	if (fd == NULL) {
		ESP_LOGI(TAG, "Save_file, Error");
		return (ESP_FAIL);
	} else {

		write_bytes = fprintf(fd, "%s", json_buff);
		if (write_bytes != strlen(json_buff)) {
			ESP_LOGE(TAG, "Failed to write file (w): %s, bytes: %d", filepath, write_bytes);
			return (ESP_FAIL);
		}
		ESP_LOGD(TAG, "Write file %s, bytes: %d", filepath, write_bytes);
		fclose(fd);
	}
	ESP_LOGI(TAG, "Save_file, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_device_load_file(config_device_t *config_device) {

	if (config_wifi_load_file(&configuration_device.wifi) != ESP_OK) {
		config_wifi_set_default(&configuration_device.wifi);
		config_wifi_save_file(&configuration_device.wifi);
	}

	if (config_sys_load_file(&configuration_device.sys) != ESP_OK) {
		config_sys_set_default(&configuration_device.sys);
		config_sys_save_file(&configuration_device.sys);
	}

	if (config_mqtt_load_file(&configuration_device.mqtt) != ESP_OK) {
		config_mqtt_set_default(&configuration_device.mqtt);
		config_mqtt_save_file(&configuration_device.mqtt);
	}

	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_wifi_load_file(config_wifi_t *config_wifi) {

	FILE *fd = NULL;
	ssize_t read_bytes;
	static char filepath[64];

	snprintf(filepath, sizeof(filepath), "%s/%s", c_base_path, CONFIG_ESP_FILE_CONFIG_WIFI);

	fd = fopen(filepath, "r");
	if (fd == NULL) {
		ESP_LOGI(TAG, "Load_file, Error");
		return (ESP_FAIL);
	}

	fseek(fd, 0L, SEEK_END); // ��������� ������ �����
	long size = ftell(fd);
	fseek(fd, 0L, SEEK_SET);
	read_bytes = fread(json_buff, 1, size, fd); // ������ ��������� ���� ���� � �����
	if (read_bytes != size) {
		ESP_LOGE(TAG, "Failed to read file (r): %s, bytes: %d", filepath, read_bytes);
		return (ESP_FAIL);
	}
	fclose(fd);
	ESP_LOGD(TAG, "Read file %s, bytes: %d", filepath, read_bytes);
//	ESP_LOGD(TAG, "Read file: %s", json_buff);

	if (config_wifi_parser_json_str(config_wifi, json_buff) != ESP_OK) {
		return (ESP_FAIL);
	}

	ESP_LOGI(TAG, "Load_file, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_sys_load_file(config_sys_t *config_sys) {

	FILE *fd = NULL;
	ssize_t read_bytes;
	static char filepath[64];

	snprintf(filepath, sizeof(filepath), "%s/%s", c_base_path, CONFIG_ESP_FILE_CONFIG_SYS);

	fd = fopen(filepath, "r");
	if (fd == NULL) {
		ESP_LOGI(TAG, "Load_file, Error");
		return (ESP_FAIL);
	}

	fseek(fd, 0L, SEEK_END); // ��������� ������ �����
	long size = ftell(fd);
	fseek(fd, 0L, SEEK_SET);
	read_bytes = fread(json_buff, 1, size, fd); // ������ ��������� ���� ���� � �����
	if (read_bytes != size) {
		ESP_LOGE(TAG, "Failed to read file (r): %s, bytes: %d", filepath, read_bytes);
		return (ESP_FAIL);
	}
	fclose(fd);
	ESP_LOGD(TAG, "Read file %s, bytes: %d", filepath, read_bytes);
//	ESP_LOGD(TAG, "Read file: %s", json_buff);

	if (config_sys_parser_json_str(config_sys, json_buff) != ESP_OK) {
		return (ESP_FAIL);
	}

	ESP_LOGI(TAG, "Load_file, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_mqtt_load_file(config_mqtt_t *config_mqtt) {

	FILE *fd = NULL;
	ssize_t read_bytes;
	static char filepath[64];

	snprintf(filepath, sizeof(filepath), "%s/%s", c_base_path, CONFIG_ESP_FILE_CONFIG_MQTT);

	fd = fopen(filepath, "r");
	if (fd == NULL) {
		ESP_LOGI(TAG, "Load_file, Error");
		return (ESP_FAIL);
	}

	fseek(fd, 0L, SEEK_END); // ��������� ������ �����
	long size = ftell(fd);
	fseek(fd, 0L, SEEK_SET);
	read_bytes = fread(json_buff, 1, size, fd); // ������ ��������� ���� ���� � �����
	if (read_bytes != size) {
		ESP_LOGE(TAG, "Failed to read file (r): %s, bytes: %d", filepath, read_bytes);
		return (ESP_FAIL);
	}
	fclose(fd);
	ESP_LOGD(TAG, "Read file %s, bytes: %d", filepath, read_bytes);
//	ESP_LOGD(TAG, "Read file: %s", json_buff);

	if (config_mqtt_parser_json_str(config_mqtt, json_buff) != ESP_OK) {
		return (ESP_FAIL);
	}

	ESP_LOGI(TAG, "Load_file, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
esp_err_t config_device_set_default(config_device_t *config_device) {

	config_wifi_set_default(&config_device->wifi);
	config_sys_set_default(&config_device->sys);
	config_mqtt_set_default(&config_device->mqtt);
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_wifi_set_default(config_wifi_t *config_wifi) {

	strlcpy(config_wifi->ssid, CONFIG_ESP_WIFI_SSID, sizeof(config_wifi->ssid));
	strlcpy(config_wifi->password, CONFIG_ESP_WIFI_PASSWORD, sizeof(config_wifi->password));
	strlcpy(config_wifi->ssid_ap, CONFIG_ESP_WIFI_SSID_AP, sizeof(config_wifi->ssid_ap));
	strlcpy(config_wifi->password_ap, CONFIG_ESP_WIFI_PASSWORD_AP, sizeof(config_wifi->password_ap));
	config_wifi->max_connection = CONFIG_MAX_STA_CONN;
	config_wifi->authmode = WIFI_AUTH_WPA_WPA2_PSK;
#if CONFIG_ESP_WIFI_MODE_AP
	config_wifi->wifi_mode= WIFI_MODE_AP;
#else
	config_wifi->wifi_mode = WIFI_MODE_STA;
#endif /*CONFIG_ESP_WIFI_MODE_AP*/
	ESP_LOGI(TAG, "Set_default, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_sys_set_default(config_sys_t *config_sys) {
	strlcpy(config_sys->host_name, CONFIG_MDNS_HOST_NAME, sizeof(config_sys->host_name));
	strlcpy(config_sys->timezone, CONFIG_ESP_TIMEZONE, sizeof(config_sys->timezone));
	strlcpy(config_sys->sntp_server, CONFIG_ESP_SNTP_SERVER, sizeof(config_sys->sntp_server));

	ESP_LOGI(TAG, "Set_default, Ok");
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t config_mqtt_set_default(config_mqtt_t *config_mqtt) {
	strlcpy(config_mqtt->uri, CONFIG_MQTT_BROKER_URL, sizeof(config_mqtt->uri));
	config_mqtt->port = CONFIG_MQTT_BROKER_PORT;
	strlcpy(config_mqtt->username, CONFIG_MQTT_BROKER_USERNAME, sizeof(config_mqtt->username));
	strlcpy(config_mqtt->password, CONFIG_MQTT_BROKER_PASSWORD, sizeof(config_mqtt->password));
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------

