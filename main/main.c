//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/dirent.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "esp_system.h"

#include "rom/ets_sys.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"

#include <sys/socket.h>
#include "lwip/apps/sntp.h"
#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"
#include "mqtt_client.h"
#include "esp_spiffs.h"
#include "mdns.h"
//
#include "esp_attr.h"
#include "esp_sleep.h"
#include "esp_spi_flash.h"
#include "driver/uart.h"
#include "rom/uart.h"
#include "sdkconfig.h"

#include "temperature_sensor.h"
#include "protect_water.h"
#include "manual_control.h"
#include "light_sensor.h"
#include "network_wifi.h"
#include "sntp_client.h"
#include "device_mqtt.h"
#include "config_device.h"
#include "spiffs_system.h"
#include "web_server.h"
#include "system.h"
//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "main";
//-----------------------------------------------------------------------------------------------
//static void light_sensor_callback_evt(int num);
int get_version();
//static void register_version();
//static int tasks_info();
//-----------------------------------------------------------------------------------------------
void app_main(void) {

	static char base_path[64];
	ESP_LOGI(TAG, "Start main");
	get_version();
//	register_version();
//	tasks_info();

	ESP_ERROR_CHECK(spiffs_system_init());		// Initialize file storage

	snprintf(base_path, sizeof(base_path), "%s%s", CONFIG_SPIFFS_MOUNT_POINT, CONFIG_SYSTEM_MOUNT_POINT);
	ESP_ERROR_CHECK(config_device_init(base_path));

	ESP_ERROR_CHECK(network_wifi_init(config_device_get_ptr()));
	ESP_ERROR_CHECK(sntp_client_init(config_device_get_ptr()));

	snprintf(base_path, sizeof(base_path), "%s%s", CONFIG_SPIFFS_MOUNT_POINT, CONFIG_WEB_MOUNT_POINT);
	ESP_ERROR_CHECK(web_server_init(base_path, config_device_get_ptr()));



	manual_control_init();
	ESP_ERROR_CHECK(protect_water_init());
	ESP_ERROR_CHECK(system_init());

	ESP_ERROR_CHECK(device_mqtt_init(config_device_get_ptr())); // �������� ����� ���� �������������


//	esp_log_level_set("*", ESP_LOG_INFO);esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

//	temperature_sensor_init();

//	light_sensor_init();
//	light_sensor_set_evt(light_sensor_callback_evt);

	vTaskDelay(60000 / portTICK_RATE_MS);
	ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
//	while (1) {
//		vTaskDelay(1000 / portTICK_RATE_MS);
//	}
}
//-----------------------------------------------------------------------------------------------


//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
/* 'version' command */
int get_version() {
	esp_chip_info_t info;
	esp_chip_info(&info);
	printf("IDF Version:%s\r\n", esp_get_idf_version());
	printf("Chip info:\r\n");
	printf("\tmodel:%s\r\n", info.model == CHIP_ESP8266 ? "ESP8266" : "Unknow");
	printf("\tcores:%d\r\n", info.cores);
	printf("\tfeature:%s%s%s%s%d%s\r\n", info.features & CHIP_FEATURE_WIFI_BGN ? "/802.11bgn" : "", info.features & CHIP_FEATURE_BLE ? "/BLE" : "",
			info.features & CHIP_FEATURE_BT ? "/BT" : "", info.features & CHIP_FEATURE_EMB_FLASH ? "/Embedded-Flash:" : "/External-Flash:",
			spi_flash_get_chip_size() / (1024 * 1024), " MB");
	printf("\trevision number:%d\r\n", info.revision);

	ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
	return 0;
}

//static void register_version() {
//	const esp_console_cmd_t cmd = { .command = "version", .help = "Get version of chip and SDK", .hint = NULL, .func = &get_version, };
//	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
//}
//
//static int tasks_info() {
//	const size_t bytes_per_task = 40; /* see vTaskList description */
//	char *task_list_buffer = malloc(uxTaskGetNumberOfTasks() * bytes_per_task);
//	if (task_list_buffer == NULL) {
//		ESP_LOGE(TAG, "failed to allocate buffer for vTaskList output");
//		return 1;
//	}
//	fputs("Task Name\tStatus\tPrio\tHWM\tTask#", stdout);
//#ifdef CONFIG_FREERTOS_VTASKLIST_INCLUDE_COREID
//    fputs("\tAffinity", stdout);
//#endif
//	fputs("\n", stdout);
//	vTaskList(task_list_buffer);
//	fputs(task_list_buffer, stdout);
//	free(task_list_buffer);
//	return 0;
//}


