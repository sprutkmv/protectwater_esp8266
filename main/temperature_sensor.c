//-----------------------------------------------------------------------------------------------
/* ds18b20_onewire.c - Retrieves readings from one or more DS18B20 temperature
 * sensors, and prints the results to stdout.
 *
 * This sample code is in the public domain.,
 */
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"

#include "temperature_sensor.h"
//-----------------------------------------------------------------------------------------------
#define GPIO_TEMPERATURE_SENSOR				(GPIO_NUM_14)
#define MAX_TEMPERATURE_SENSOR 				(8)
#define RESCAN_INTERVAL_TEMPERATURE_SENSOR 	(8)
#define LOOP_DELAY_MS_TEMPERATURE_SENSOR 	(2500)
//-----------------------------------------------------------------------------------------------
static const char *TAG = "temperature_sensor";


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void temperature_sensor_scan(void *pvParameters) {
    ds18b20_addr_t addrs[MAX_TEMPERATURE_SENSOR];
    float temps[MAX_TEMPERATURE_SENSOR];
    int sensor_count;
    
    // There is no special initialization required before using the ds18b20
    // routines.  However, we make sure that the internal pull-up resistor is
    // enabled on the GPIO pin so that one can connect up a sensor without
    // needing an external pull-up (Note: The internal (~47k) pull-ups of the
    // ESP8266 do appear to work, at least for simple setups (one or two sensors
    // connected with short leads), but do not technically meet the pull-up
    // requirements from the DS18B20 datasheet and may not always be reliable.
    // For a real application, a proper 4.7k external pull-up resistor is
    // recommended instead!)

    gpio_set_pull_mode(GPIO_TEMPERATURE_SENSOR, GPIO_PULLUP_ONLY);

    while(1) {
        // Every RESCAN_INTERVAL_TEMPERATURE_SENSOR samples, check to see if the sensors connected
        // to our bus have changed.
        sensor_count = ds18b20_scan_devices(GPIO_TEMPERATURE_SENSOR, addrs, MAX_TEMPERATURE_SENSOR);

        if (sensor_count < 1) {
        	ESP_LOGI(TAG, "No sensors detected!\n");
        	vTaskDelay(LOOP_DELAY_MS_TEMPERATURE_SENSOR / portTICK_PERIOD_MS);
        }
        else {
        	ESP_LOGI(TAG,"%d sensors detected:\n", sensor_count);
            // If there were more sensors found than we have space to handle,
            // just report the first MAX_TEMPERATURE_SENSOR..
            if (sensor_count > MAX_TEMPERATURE_SENSOR) sensor_count = MAX_TEMPERATURE_SENSOR;

            // Do a number of temperature samples, and print the results.
            for (int i = 0; i < RESCAN_INTERVAL_TEMPERATURE_SENSOR; i++) {
                ds18b20_measure_and_read_multi(GPIO_TEMPERATURE_SENSOR, addrs, sensor_count, temps);
                for (int j = 0; j < sensor_count; j++) {
                    // The DS18B20 address is a 64-bit integer, but newlib-nano
                    // printf does not support printing 64-bit values, so we
                    // split it up into two 32-bit integers and print them
                    // back-to-back to make it look like one big hex number.
                    uint32_t addr0 = addrs[j] >> 32;
                    uint32_t addr1 = addrs[j];
                    float temp_c = temps[j];
                    float temp_f = (temp_c * 1.8) + 32;
                    ESP_LOGI(TAG,"  Sensor %08x%08x reports %f deg C (%f deg F)\n", addr0, addr1, temp_c, temp_f);
                }
                ESP_LOGI(TAG,"\n");

                // Wait for a little bit between each sample (note that the
                // ds18b20_measure_and_read_multi operation already takes at
                // least 750ms to run, so this is on top of that delay).
                vTaskDelay(LOOP_DELAY_MS_TEMPERATURE_SENSOR / portTICK_PERIOD_MS);
            }
        }
    }
}

void temperature_sensor_init(void) {

    xTaskCreate(&temperature_sensor_scan, "temperature_sensor_scan", 1024, NULL, 2, NULL);
}

