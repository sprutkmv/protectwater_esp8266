//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"

#include "protect_water_tap.h"
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "protect_water_tap";
static xQueueHandle protect_wate_tap_queue = NULL;
static state_tap_t state_tap = STATE_TAP_OPEN;
static void (*protect_water_tap_status_evt)(state_tap_t) = NULL;
//-----------------------------------------------------------------------------------------------
//void protect_water_tap_init();
void protect_water_tap_init_gpio();
void protect_water_tap_open_cmd();
void protect_water_tap_close_cmd();
void protect_water_tap_idle_cmd();
void protect_water_tap_set_status(state_tap_t state);
void protect_water_tap_task();
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
void protect_water_tap_init() {
	protect_water_tap_init_gpio();
	protect_wate_tap_queue = xQueueCreate(1, sizeof(cmd_tap_t));
	xTaskCreate(protect_water_tap_task, "protect_water_tap_task", 1024, NULL, 2, NULL);
}
//-----------------------------------------------------------------------------------------------
void protect_water_tap_open() {
	cmd_tap_t cmd_tap;
	cmd_tap = CMD_TAP_OPEN;
	xQueueSend(protect_wate_tap_queue, &cmd_tap, 0);
}
//-----------------------------------------------------------------------------------------------
void protect_water_tap_close() {
	cmd_tap_t cmd_tap;
	cmd_tap = CMD_TAP_CLOSE;
	xQueueSend(protect_wate_tap_queue, &cmd_tap, 0);
}
//-----------------------------------------------------------------------------------------------
state_tap_t protect_water_tap_get_status() {
	return (state_tap);
}
//-----------------------------------------------------------------------------------------------
void protect_water_tap_status_set_evt(void (*evt)(state_tap_t)) {
	protect_water_tap_status_evt = evt;
}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
void protect_water_tap_task() {
	static TickType_t time_old = 0;
	static cmd_tap_t cmd_tap = CMD_TAP_OPEN;
	for (;;) {
		if (xQueueReceive(protect_wate_tap_queue, &cmd_tap, (1000 / portTICK_RATE_MS))) {
			ESP_LOGI(TAG, "cmd_tap: [%d]", cmd_tap);
		}

		if (cmd_tap == CMD_TAP_OPEN) {
			if ((state_tap == STATE_TAP_CLOSE) || (state_tap == STATE_TAP_CLOSES)) {
				protect_water_tap_set_status(STATE_TAP_OPENS);
				protect_water_tap_open_cmd();
				time_old = xTaskGetTickCount();
			} else if (state_tap == STATE_TAP_OPENS) {
				TickType_t time_temp;
				time_temp = xTaskGetTickCount();
				if (((time_temp - time_old) * portTICK_RATE_MS) >= PW_DELAY_OPENS) {
					protect_water_tap_idle_cmd();
					protect_water_tap_set_status(STATE_TAP_OPEN);
				}
			}
		} else if (cmd_tap == CMD_TAP_CLOSE) {
			if ((state_tap == STATE_TAP_OPEN) || (state_tap == STATE_TAP_OPENS)) {
				protect_water_tap_set_status(STATE_TAP_CLOSES);
				protect_water_tap_close_cmd();
				time_old = xTaskGetTickCount();
			} else if (state_tap == STATE_TAP_CLOSES) {
				TickType_t time_temp;
				time_temp = xTaskGetTickCount();
				if (((time_temp - time_old) * portTICK_RATE_MS) >= PW_DELAY_CLOSES) {
					protect_water_tap_idle_cmd();
					protect_water_tap_set_status(STATE_TAP_CLOSE);
				}
			}
		}
	}
}
//-----------------------------------------------------------------------------------------------
void protect_water_tap_open_cmd() {
	gpio_set_level(PW_GPIO_OUTPUT_CLOSE, PW_GPIO_OUTPUT_PASSIVE_LEVEL);
	vTaskDelay(PW_DELAY_DEADTIME_MS / portTICK_RATE_MS);
	gpio_set_level(PW_GPIO_OUTPUT_OPEN, PW_GPIO_OUTPUT_ACTIVE_LEVEL);
	ESP_LOGI(TAG, "protect_water_tap_open");
}
//-----------------------------------------------------------------------------------------------
void protect_water_tap_close_cmd() {
	gpio_set_level(PW_GPIO_OUTPUT_OPEN, PW_GPIO_OUTPUT_PASSIVE_LEVEL);
	vTaskDelay(PW_DELAY_DEADTIME_MS / portTICK_RATE_MS);
	gpio_set_level(PW_GPIO_OUTPUT_CLOSE, PW_GPIO_OUTPUT_ACTIVE_LEVEL);
	ESP_LOGI(TAG, "protect_water_tap_close");
}

//-----------------------------------------------------------------------------------------------
void protect_water_tap_idle_cmd() {
	gpio_set_level(PW_GPIO_OUTPUT_OPEN, PW_GPIO_OUTPUT_PASSIVE_LEVEL);
	vTaskDelay(PW_DELAY_DEADTIME_MS / portTICK_RATE_MS);
	gpio_set_level(PW_GPIO_OUTPUT_CLOSE, PW_GPIO_OUTPUT_PASSIVE_LEVEL);
	ESP_LOGI(TAG, "protect_water_tap_idle");
}
//-----------------------------------------------------------------------------------------------
void protect_water_tap_set_status(state_tap_t state) {
	state_tap = state;
	if (protect_water_tap_status_evt != NULL) {
		protect_water_tap_status_evt(state_tap);
	}
}
//-----------------------------------------------------------------------------------------------
void protect_water_tap_init_gpio() {
	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE;	//disable interrupt
	io_conf.mode = GPIO_MODE_OUTPUT;		//set as output mode
	io_conf.pin_bit_mask = PW_GPIO_OUTPUT_PIN_SEL;		//bit mask of the pins that you want to set,
	io_conf.pull_down_en = 0;	//disable pull-down mode
	io_conf.pull_up_en = 0;		//disable pull-up mode
	gpio_config(&io_conf);		//configure GPIO with the given settings
}
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
