/*
 /  ���������� ����������

 */
//------------------------------------------------------------------------------
// Include
//------------------------------------------------------------------------------
#include "keyboard.h"

static TButtons buttons[MAX_NUM_BUTTONS];
void ScanButton(uint8_t NumButton);
uint8_t GetStatePinButtons(TPinButtons *PinButtons);
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ������� �������������
//------------------------------------------------------------------------------
void InitKeyBoard() {
	buttons[0].pin.GPIO_Pin = GPIO_INPUT_BUTTON;
	buttons[0].pin.max_count_dreb = MAX_COUNT_DREB;
	buttons[0].key_state = KEY_NOT;
	buttons[0].count_timer = 0;

	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE;	//disable interrupt
//	io_conf.intr_type = GPIO_INTR_NEGEDGE;		//interrupt of rising edge
	io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;	//bit mask of the pins, use GPIO4/5 here
	io_conf.mode = GPIO_MODE_INPUT;		//set as input mode
	io_conf.pull_up_en = 1;				//enable pull-up mode
	gpio_config(&io_conf);
}
//------------------------------------------------------------------------------
void ScanKeyBoard() {
	uint8_t i;
	for (i = 0; i < MAX_NUM_BUTTONS; i++) {
		ScanButton(i);
	}
}
//------------------------------------------------------------------------------
TStateButton GetStateButton(uint8_t NumButton) {
	TStateButton key_state;
	key_state = buttons[NumButton].key_state;
	buttons[NumButton].key_state = KEY_NOT;
	return (key_state);
}
void ResetStateButton(uint8_t NumButton) {
	buttons[NumButton].old_key = 0;
	buttons[NumButton].count_timer = 0;
	buttons[NumButton].key_state = KEY_NOT;
}
//------------------------------------------------------------------------------
void ScanButton(uint8_t NumButton) {
	uint8_t key;
	key = GetStatePinButtons(&buttons[NumButton].pin);

	if (buttons[NumButton].count_timer) {
		buttons[NumButton].count_timer--;
	}
	// ������� ������
	if ((key == 1) & (key != buttons[NumButton].old_key)) {
		buttons[NumButton].count_timer = MAX_TIME_DELAY_BUTTON;
		buttons[NumButton].key_state = KEY_DOWN;
	} // ���������� ������
	else if ((key == 0) & (key != buttons[NumButton].old_key)) {
		if (buttons[NumButton].count_timer) {
			buttons[NumButton].key_state = KEY_UP;
		}
		buttons[NumButton].count_timer = 0;
	} // ������ ���������
	else if (buttons[NumButton].count_timer == 1) {
		buttons[NumButton].key_state = KEY_DELAY;
	}
	buttons[NumButton].old_key = key;
}
//------------------------------------------------------------------------------
uint8_t GetStatePinButtons(TPinButtons *PinButtons) {
	// ������ �� ������ 1
	if (!gpio_get_level(PinButtons->GPIO_Pin)) {
		//��, ��������� �� ����� ���������� ��������?
		if (PinButtons->count_dreb > PinButtons->max_count_dreb) {
			PinButtons->key_press = 1;        // ���������� ���� ���������
		} else {
			PinButtons->count_dreb++;      //�� ��������,  ���������� ����������
		}
	} else {
		PinButtons->key_press = 0;  // �������� ���� ���������
		PinButtons->count_dreb = 0; // �������� ������� ���������� ���������� ��������
	}
	return (PinButtons->key_press);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
