/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : andreyshavlikov@gmail.com
 * License public domain/CC0
 */
#ifndef __NETWORK_WIFI_H
#define __NETWORK_WIFI_H
//-----------------------------------------------------------------------------------------------
#include "config_device.h"
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------
///* The examples use simple WiFi configuration that you can set via
// 'make menuconfig'.
//
// If you'd rather not, just change the below entries to strings with
// the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
// */
//#define EXAMPLE_ESP_WIFI_MODE_AP   CONFIG_ESP_WIFI_MODE_AP //TRUE:AP FALSE:STA
//#define EXAMPLE_ESP_WIFI_SSID      "sprut_esp"
//#define EXAMPLE_ESP_WIFI_PASS      "strelnikova"
////#define EXAMPLE_ESP_WIFI_SSID      "sprut_lan"
////#define EXAMPLE_ESP_WIFI_PASS      "strelnikova"
//#define EXAMPLE_MAX_STA_CONN       CONFIG_MAX_STA_CONN

/* The event group allows multiple bits for each event,
 but we only care about one event - are we connected
 to the AP with an IP? */
#define  WIFI_CONNECTED_BIT  BIT0
//-----------------------------------------------------------------------------------------------
/* FreeRTOS event group to signal when we are connected*/
extern EventGroupHandle_t wifi_event_group;



//-----------------------------------------------------------------------------------------------
esp_err_t network_wifi_init(config_device_t *config_device);





//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* __NETWORK_WIFI_H */
//-----------------------------------------------------------------------------------------------
