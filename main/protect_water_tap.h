/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : ghetman@gmail.com
 * License public domain/CC0
 */
#ifndef PROTECT_WATER_TAP_H_
#define PROTECT_WATER_TAP_H_
//-----------------------------------------------------------------------------------------------
#include "driver/gpio.h"
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------

#define PW_DELAY_OPENS					(10000)
#define PW_DELAY_CLOSES					(10000)
#define PW_DELAY_DEADTIME_MS 			(10)
#define PW_GPIO_OUTPUT_ACTIVE_LEVEL  	(1)
#define PW_GPIO_OUTPUT_PASSIVE_LEVEL  	(0)

#define PW_GPIO_OUTPUT_CLOSE  		(GPIO_NUM_15)
#define PW_GPIO_OUTPUT_OPEN  		(GPIO_NUM_12)
#define PW_GPIO_OUTPUT_PIN_SEL  ((1ULL<<PW_GPIO_OUTPUT_CLOSE) | (1ULL<<PW_GPIO_OUTPUT_OPEN))

//-----------------------------------------------------------------------------------------------
typedef enum _state_tap_t {
//	STATE_TAP_ERROR = -1,
	STATE_TAP_CLOSE = 0,
	STATE_TAP_OPEN = 1,
	STATE_TAP_CLOSES = 2,
	STATE_TAP_OPENS = 3
} state_tap_t;

typedef enum _cmd_tap_t {
//	CMD_TAP_ERROR = -1,
	CMD_TAP_CLOSE = 0,
	CMD_TAP_OPEN = 1,
} cmd_tap_t;

//-----------------------------------------------------------------------------------------------
void protect_water_tap_init();
void protect_water_tap_open();
void protect_water_tap_close();
state_tap_t protect_water_tap_get_status();
void protect_water_tap_status_set_evt(void (*evt)(state_tap_t));
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* PROTECT_WATER_TAP_H_ */
//-----------------------------------------------------------------------------------------------
