/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : ghetman@gmail.com
 * License public domain/CC0
 */
#ifndef PROTECT_WATER_SENSOR_H_
#define PROTECT_WATER_SENSOR_H_
//-----------------------------------------------------------------------------------------------
#include "driver/gpio.h"
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------
#define	PWS_MAX_COUNT_SENSORS		(3)
#define PWS_GPIO_INPUT_PWS_SENSOR_1	(GPIO_NUM_0)
#define PWS_GPIO_INPUT_PWS_SENSOR_2	(GPIO_NUM_0)
#define PWS_GPIO_INPUT_PWS_SENSOR_3	(GPIO_NUM_0)
#define PWS_GPIO_INPUT_PIN_SEL  	((1ULL<<PWS_GPIO_INPUT_PWS_SENSOR_1) | (1ULL<<PWS_GPIO_INPUT_PWS_SENSOR_2)| (1ULL<<PWS_GPIO_INPUT_PWS_SENSOR_3))

#define	PWS_MAX_COUNT_DREB			(10)		// ������ ������ pin �������
#define PWS_LOOP_MS					(500)
//-----------------------------------------------------------------------------------------------
typedef struct {
	uint32_t GPIO_Pin;
	uint32_t count_dreb;
	uint32_t max_count_dreb;
	uint8_t key_press;
} pin_sensor_t;

typedef enum _state_sensor_t {
	STATE_SENSOR_ERROR = -1,
	STATE_SENSOR_NORMAL = 0,
	STATE_SENSOR_ALARM=1
} state_sensor_t;

typedef struct {
	pin_sensor_t pin;
	state_sensor_t state;
} sensor_t;
//-----------------------------------------------------------------------------------------------
// Public
void protect_water_sensor_init();
state_sensor_t protect_water_sensor_get_state(uint32_t num);
uint32_t protect_water_sensor_max_num();

//void protect_water_sensor_set_evt(xQueueHandle evt_queue);
void protect_water_sensor_set_evt(void (*evt)(int)) ;
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* PROTECT_WATER_SENSOR_H_ */
//-----------------------------------------------------------------------------------------------
