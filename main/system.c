//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "esp_system.h"
#include "driver/adc.h"
#include "device_mqtt.h"

#include "system.h"

//-----------------------------------------------------------------------------------------------
#define MAIN_MQTT_SUBSCRIBE_SYSTEM "/pw/system"
//-----------------------------------------------------------------------------------------------
static const char *TAG = "system";
static xQueueHandle system_mqtt_rx_queue = NULL;
//-----------------------------------------------------------------------------------------------
static void system_mqtt_rx_task();
void system_scheduler_mqtt(char *data);
void system_send_mqtt_free_heap();
void system_send_mqtt_idf_version();
void system_send_mqtt_flash_size_map();
void system_send_mqtt_chip_info();
void system_send_mqtt_voltage();
void system_adc_voltage_init();
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
esp_err_t system_init() {
	BaseType_t answer = xTaskCreate(system_mqtt_rx_task, "system_mqtt_rx_task", 2048, NULL, 2, NULL);
	if (answer != pdPASS) {
		ESP_LOGE(TAG, "Error xTaskCreate: system_mqtt_rx_task");
		return (ESP_FAIL);
	}
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void system_mqtt_rx_task() {

	system_adc_voltage_init();

	system_mqtt_rx_queue = xQueueCreate(10, sizeof(mqtt_packet_data_t));
	if (system_mqtt_rx_queue == NULL) {
		ESP_LOGE(TAG, "Error xQueueCreate: system_mqtt_rx_queue");
	}

	device_mqtt_subscribe(MAIN_MQTT_SUBSCRIBE_SYSTEM, system_mqtt_rx_queue);

	mqtt_packet_data_t packet_data;
	for (;;) {
		if (xQueueReceive(system_mqtt_rx_queue, &packet_data, portMAX_DELAY)) {
			ESP_LOGI(TAG, "system_mqtt_packet_rx: data= %s", packet_data.data);
			system_scheduler_mqtt(packet_data.data);
		}
	}
}
void system_adc_voltage_init() {
	adc_config_t adc_config;
	// Depend on menuconfig->Component config->PHY->vdd33_const value
	// When measuring system voltage(ADC_READ_VDD_MODE), vdd33_const must be set to 255.
	adc_config.mode = ADC_READ_VDD_MODE;
	adc_config.clk_div = 8; // ADC sample collection clock = 80MHz/clk_div = 10MHz
	ESP_ERROR_CHECK(adc_init(&adc_config));
}
//-----------------------------------------------------------------------------------------------
void system_scheduler_mqtt(char *data) {
	if (strcmp(data, "free_heap") == 0) {
		system_send_mqtt_free_heap();
	} else if (strcmp(data, "idf_version") == 0) {
		system_send_mqtt_idf_version();
	} else if (strcmp(data, "flash_size_map") == 0) {
		system_send_mqtt_flash_size_map();
	} else if (strcmp(data, "chip_info") == 0) {
		system_send_mqtt_chip_info();
	} else if (strcmp(data, "voltage") == 0) {
		system_send_mqtt_voltage();
	}
}
//-----------------------------------------------------------------------------------------------
void system_send_mqtt_voltage() {
	mqtt_packet_t mqtt_packet;
	uint16_t adc_data;
	if (ESP_OK == adc_read(&adc_data)) {
		ESP_LOGI(TAG, "adc read: %d", adc_data);
		strlcpy(mqtt_packet.topic, "/pw/system/voltage", sizeof(mqtt_packet.topic));
		snprintf(mqtt_packet.data, sizeof(mqtt_packet.data), "%d", adc_data);
		device_mqtt_published(&mqtt_packet);
	}
}
//-----------------------------------------------------------------------------------------------
void system_send_mqtt_free_heap() {
	mqtt_packet_t mqtt_packet;
	uint32_t free_heap_size;
	free_heap_size = esp_get_free_heap_size();
	ESP_LOGI(TAG, "free heap size: %d", free_heap_size);
	strlcpy(mqtt_packet.topic, "/pw/system/free_heap", sizeof(mqtt_packet.topic));
	snprintf(mqtt_packet.data, sizeof(mqtt_packet.data), "%d", free_heap_size);
	device_mqtt_published(&mqtt_packet);
}
//-----------------------------------------------------------------------------------------------
void system_send_mqtt_idf_version() {
	mqtt_packet_t mqtt_packet;
	ESP_LOGI(TAG, "idf version: %s", esp_get_idf_version());
	strlcpy(mqtt_packet.topic, "/pw/system/idf_version", sizeof(mqtt_packet.topic));
	snprintf(mqtt_packet.data, sizeof(mqtt_packet.data), "%s", esp_get_idf_version());
	device_mqtt_published(&mqtt_packet);
}
//-----------------------------------------------------------------------------------------------
void system_send_mqtt_flash_size_map() {
	mqtt_packet_t mqtt_packet;
	uint32_t flash_size_map;
	flash_size_map = system_get_flash_size_map();
	ESP_LOGI(TAG, "flash size map: %d", flash_size_map);
	strlcpy(mqtt_packet.topic, "/pw/system/flash_size_map", sizeof(mqtt_packet.topic));
	snprintf(mqtt_packet.data, sizeof(mqtt_packet.data), "%d", flash_size_map);
	device_mqtt_published(&mqtt_packet);
}
//-----------------------------------------------------------------------------------------------
void system_send_mqtt_chip_info() {
	mqtt_packet_t mqtt_packet;
	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);
	ESP_LOGI(TAG, "chip_info:\n  model:[%d]\n  features:[%d]\n  cores:[%d]\n  revision:[%d]\n", chip_info.model, chip_info.features, chip_info.cores, chip_info.revision);
	strlcpy(mqtt_packet.topic, "/pw/system/chip_info", sizeof(mqtt_packet.topic));
	snprintf(mqtt_packet.data, sizeof(mqtt_packet.data), "model:[%d], features:[%d], cores:[%d], revision:[%d]", chip_info.model, chip_info.features, chip_info.cores,
			chip_info.revision);
	device_mqtt_published(&mqtt_packet);
}
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------

