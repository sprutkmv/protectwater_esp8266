#ifndef KEYBOARD_H
#define KEYBOARD_H

//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------
#include "driver/gpio.h"
//------------------------------------------------------------------------------
#define GPIO_INPUT_BUTTON  		(GPIO_NUM_4)
#define GPIO_INPUT_PIN_SEL  	((1ULL<<GPIO_INPUT_BUTTON))

#define MAX_NUM_BUTTONS			(1)
#define	MAX_TIME_DELAY_BUTTON	(200)		// ������ ������
#define	MAX_COUNT_DREB			(10)		// ������ ������ pin �������


#define	BUTTON_1				(0)
//------------------------------------------------------------------------------
typedef struct
{
  uint16_t  GPIO_Pin;
  uint32_t  count_dreb;
  uint32_t  max_count_dreb;
  uint8_t  key_press;
}
TPinButtons;

typedef enum	_TStateButton{ KEY_NOT= 0, KEY_UP, KEY_DELAY, KEY_DOWN}TStateButton;

typedef struct
{
	TPinButtons	pin;
	TStateButton key_state;
	uint8_t old_key;
	uint32_t count_timer;
}
TButtons;
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  void InitKeyBoard();
  void ScanKeyBoard();
//  uint8_t GetValueKey();

  TStateButton GetStateButton(uint8_t NumButton);
  void	ResetStateButton(uint8_t NumButton);
//------------------------------------------------------------------------------                            
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  //-----------------------------------------------------------------------------------------------
  #ifdef	__cplusplus
  }
  #endif

#endif 

