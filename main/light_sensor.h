/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : ghetman@gmail.com
 * License public domain/CC0
 */
#ifndef __LIGHT_SENSOR_H
#define __LIGHT_SENSOR_H
//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------------------
void light_sensor_init();
//void light_sensor_set_evt(void (*evt)(int)) ;
//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* __LIGHT_SENSOR_H */
//-----------------------------------------------------------------------------------------------
