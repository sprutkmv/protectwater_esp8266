//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "esp_log.h"
#include "esp_system.h"

#include "esp_spiffs.h"
#include "esp_vfs.h"

#include "spiffs_system.h"
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "spiffs_system";
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
esp_err_t spiffs_system_init() {

	ESP_LOGI(TAG, "Initializing SPIFFS");
	esp_vfs_spiffs_conf_t conf;
//	strlcpy(conf.base_path, base_path, sizeof(conf.base_path));
	conf.base_path = CONFIG_SPIFFS_MOUNT_POINT;
	conf.partition_label = NULL;
	conf.max_files = 5;
	conf.format_if_mount_failed = true;

	esp_err_t ret = esp_vfs_spiffs_register(&conf);
	if (ret != ESP_OK) {
		if (ret == ESP_FAIL) {
			ESP_LOGE(TAG, "Failed to mount or format filesystem");
		} else if (ret == ESP_ERR_NOT_FOUND) {
			ESP_LOGE(TAG, "Failed to find SPIFFS partition");
		} else {
			ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
		}
		return ESP_FAIL;
	}

	size_t total = 0, used = 0;
	ret = esp_spiffs_info(NULL, &total, &used);
	if (ret != ESP_OK) {
		ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
		return ESP_FAIL;
	}
	ESP_LOGI(TAG, "Partition size: total: %d, used: %d", total, used);

	DIR *dir;
	struct dirent *entry;
	uint32_t i=0;
	dir = opendir(CONFIG_SPIFFS_MOUNT_POINT);
	ESP_LOGI(TAG, "Files dir :%s", CONFIG_SPIFFS_MOUNT_POINT);
	if (!dir) {
		ESP_LOGI(TAG, "Files dir : error");
	} else {
		while ((entry = readdir(dir)) != NULL) {
			i++;
			ESP_LOGI(TAG, "  [%d]   %d - %s [%d]", i,  entry->d_ino, entry->d_name, entry->d_type);
		};
		closedir(dir);
	}
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

