/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : ghetman@gmail.com
 * License public domain/CC0
 */
#ifndef __device_mqtt_H
#define __device_mqtt_H
//-----------------------------------------------------------------------------------------------
#include "mqtt_client.h"
#include "config_device.h"
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------
#define MQTT_SIZE_TORIC 	(MQTT_MAX_LWT_TOPIC)
#define MQTT_SIZE_DATA		(MQTT_MAX_LWT_MSG)
//-----------------------------------------------------------------------------------------------
typedef struct {
	char topic[MQTT_SIZE_TORIC];
} mqtt_packet_topic_t;

typedef struct {
	char data[MQTT_SIZE_DATA];
} mqtt_packet_data_t;

typedef struct {
	char topic[MQTT_SIZE_TORIC];
	char data[MQTT_SIZE_DATA];
} mqtt_packet_t;


typedef struct {
	char topic[MQTT_SIZE_TORIC];
	xQueueHandle * rx_queue;
} subscribe_t;


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
esp_err_t device_mqtt_init(config_device_t *config_device);
esp_err_t device_mqtt_published(mqtt_packet_t * mqtt_packet);
esp_err_t device_mqtt_subscribe(char *topic, xQueueHandle rx_queue);
//void device_mqtt_set_rx_queue(xQueueHandle *queue);
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* __device_mqtt_H */
//-----------------------------------------------------------------------------------------------
