//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "esp_system.h"

#include "rom/ets_sys.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"

#include <netdb.h>
#include <sys/socket.h>

#include "network_wifi.h"
#include "device_mqtt.h"
//-----------------------------------------------------------------------------------------------
#define MAX_CNT_SUBSCRIBE		(10)

//-----------------------------------------------------------------------------------------------
static const char *TAG = "device_mqtt";
static config_mqtt_t *config_mqtt_ptr;
xQueueHandle mqtt_rx_queue = NULL;
xQueueHandle mqtt_tx_queue = NULL;

static esp_mqtt_client_handle_t mqtt_client = NULL;

subscribe_t subscribe[MAX_CNT_SUBSCRIBE] = { 0 };
static uint32_t count_subscribe = 0;

//-----------------------------------------------------------------------------------------------
static void device_mqtt_task();
static esp_err_t device_mqtt_event_handler(esp_mqtt_event_handle_t event);
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
// ����� ��������, ������ ���� ��������� ��� ��������
esp_err_t device_mqtt_init(config_device_t *config_device) {

	ESP_LOGI(TAG, "Initializing MQTT Client");
	config_mqtt_ptr = &config_device->mqtt;

	BaseType_t answer;
	answer = xTaskCreate(device_mqtt_task, "device_mqtt_task", 2048, NULL, 2, NULL);
	if (answer != pdPASS) {
		ESP_LOGE(TAG, "Error xTaskCreate: mqtt_task");
		return (ESP_FAIL);
	}
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
esp_err_t device_mqtt_published(mqtt_packet_t *mqtt_packet) {
	if (mqtt_tx_queue != NULL) {
		xQueueSend(mqtt_tx_queue, mqtt_packet, NULL);
		return (ESP_OK);
	}
	return (ESP_FAIL);
}
//-----------------------------------------------------------------------------------------------
// ������������� ����� �� ������� mqtt
esp_err_t device_mqtt_subscribe(char *topic, xQueueHandle rx_queue) {

	if (count_subscribe < MAX_CNT_SUBSCRIBE) {
		// ������ �� ������
		if (topic[0])  {
			if (rx_queue != NULL) {
				strlcpy(subscribe[count_subscribe].topic, topic, sizeof(subscribe[count_subscribe].topic));
				subscribe[count_subscribe].rx_queue = rx_queue;
//				int msg_id = esp_mqtt_client_subscribe(mqtt_client, subscribe[count_subscribe].topic, 0);
//				if (msg_id < 0) {
//					ESP_LOGE(TAG, "subscribe unsuccessful: %s, msg_id=%d", subscribe[count_subscribe].topic, msg_id);
//				} else {
//					ESP_LOGI(TAG, "subscribe successful: %s, msg_id=%d", subscribe[count_subscribe].topic, msg_id);
//				}
			}else{
				ESP_LOGE(TAG, "Subscribe Topic: %s, Receive Queue NULL", topic);
			}
		}else{
			ESP_LOGE(TAG, "Subscribe Topic: %s, Empty", topic);
		}
		count_subscribe++;
		return (ESP_OK);
	} else {
		return (ESP_FAIL);
	}
}
//-----------------------------------------------------------------------------------------------
//void device_mqtt_set_rx_queue(xQueueHandle *queue) {
//	mqtt_rx_queue = queue;
//}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void device_mqtt_task() {
	mqtt_packet_t mqtt_packet;
	esp_mqtt_client_config_t mqtt_cfg;
	xEventGroupWaitBits(wifi_event_group, WIFI_CONNECTED_BIT, false, true, portMAX_DELAY);

	memset(&mqtt_cfg, 0, sizeof(mqtt_cfg));
	mqtt_cfg.uri = &config_mqtt_ptr->uri;
	mqtt_cfg.port = config_mqtt_ptr->port;
	mqtt_cfg.username = &config_mqtt_ptr->username;
	mqtt_cfg.password = &config_mqtt_ptr->password;
	mqtt_cfg.event_handle = device_mqtt_event_handler;

#if CONFIG_BROKER_URL_FROM_STDIN
	    char line[128];

	    if (strcmp(mqtt_cfg.uri, "FROM_STDIN") == 0) {
	        int count = 0;
	        printf("Please enter url of mqtt broker\n");
	        while (count < 128) {
	            int c = fgetc(stdin);
	            if (c == '\n') {
	                line[count] = '\0';
	                break;
	            } else if (c > 0 && c < 127) {
	                line[count] = c;
	                ++count;
	            }
	            vTaskDelay(10 / portTICK_PERIOD_MS);
	        }
	        mqtt_cfg.uri = line;
	        printf("Broker url: %s\n", line);
	    } else {
	        ESP_LOGE(TAG, "Configuration mismatch: wrong broker url");
	        abort();
	    }
	#endif /* CONFIG_BROKER_URL_FROM_STDIN */

	mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
	esp_mqtt_client_start(mqtt_client);

	mqtt_tx_queue = xQueueCreate(10, sizeof(mqtt_packet_t));
	if (mqtt_tx_queue == NULL) {
		ESP_LOGE(TAG, "Error xQueueCreate: mqtt_tx_queue");
	}

	for (;;) {
		if (xQueueReceive(mqtt_tx_queue, &mqtt_packet, portMAX_DELAY)) {
			ESP_LOGI(TAG, "mqtt_packet_tx: topic= %s, data= %s", mqtt_packet.topic, mqtt_packet.data);
			esp_mqtt_client_publish(mqtt_client, mqtt_packet.topic, mqtt_packet.data, 0, 0, 0);
		}
	}
}
//-----------------------------------------------------------------------------------------------
static esp_err_t device_mqtt_event_handler(esp_mqtt_event_handle_t event) {
	esp_mqtt_client_handle_t client = event->client;
	int msg_id;
	mqtt_packet_t mqtt_packet;
	// your_context_t *context = event->context;
	switch (event->event_id) {
	case MQTT_EVENT_CONNECTED:
		ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
		for (uint32_t i = 0; i < count_subscribe; i++) {
			if (subscribe[i].topic[0]) {
				msg_id = esp_mqtt_client_subscribe(client, subscribe[i].topic, 0);
				if (msg_id < 0) {
					ESP_LOGE(TAG, "Subscribe unsuccessful: %s, msg_id=%d", subscribe[i].topic, msg_id);
				} else {
					ESP_LOGI(TAG, "Subscribe successful: %s, msg_id=%d", subscribe[i].topic, msg_id);
				}

			}else{
				ESP_LOGE(TAG, "Subscribe Topic: %s, Empty", subscribe[i].topic);
			}
		}
		break;
	case MQTT_EVENT_DISCONNECTED:
		ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
		break;
	case MQTT_EVENT_SUBSCRIBED:
		ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
//		msg_id = esp_mqtt_client_publish(client, "/topic/les", "gren", 0, 0, 0);
//		ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
		break;
	case MQTT_EVENT_UNSUBSCRIBED:
		ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
		break;
	case MQTT_EVENT_PUBLISHED:
		ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
		break;
	case MQTT_EVENT_DATA:
		ESP_LOGI(TAG, "MQTT_EVENT_DATA");
		ESP_LOGI(TAG, "TOPIC=%.*s, DATA=%.*s", event->topic_len, event->topic, event->data_len, event->data);

		if (event->topic_len < sizeof(mqtt_packet.topic)) {
			snprintf(mqtt_packet.topic, event->topic_len+1, "%s", event->topic);

			if (event->data_len < sizeof(mqtt_packet.data)) {
				snprintf(mqtt_packet.data, event->data_len+1, "%s", event->data);
				for (uint32_t i = 0; i < count_subscribe; i++) {
//					ESP_LOGI(TAG, "topic=%s, subscribe.topic=%s", mqtt_packet.topic, subscribe[i].topic);
					if (strcmp(mqtt_packet.topic, subscribe[i].topic) == 0) {
						if (subscribe[i].rx_queue != NULL) {
							if (xQueueSend(subscribe[i].rx_queue, mqtt_packet.data, 0) != pdPASS) {
								ESP_LOGE(TAG, "Topic: %s, Receive Queue if Full", mqtt_packet.topic);
							}
						} else {
							ESP_LOGE(TAG, "Topic: %s, Receive Queue NULL", mqtt_packet.topic);
						}
					}
				}
			} else {
				ESP_LOGE(TAG, "(data_len:%d) !< (sizeof(mqtt_packet.data): %d)", event->data_len, sizeof(mqtt_packet.data));
			}
		} else {
			ESP_LOGE(TAG, "(topic_len: %d) !< (sizeof(mqtt_packet.topic): %d)", event->topic_len, sizeof(mqtt_packet.topic));
		}
		break;
	case MQTT_EVENT_ERROR:
		ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
		break;
	}
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
