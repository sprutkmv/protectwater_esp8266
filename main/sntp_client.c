//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_log.h"
#include "esp_system.h"

#include "rom/ets_sys.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"

#include <netdb.h>
#include <sys/socket.h>
#include "lwip/apps/sntp.h"

#include "network_wifi.h"
#include "sntp_client.h"
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
static const char *TAG = "sntp_client";
static config_sys_t *config_sys_ptr;
//-----------------------------------------------------------------------------------------------
void sntp_client_initialize(void);
void sntp_client_obtain_time(void);
static void sntp_client_task(void *arg);
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
esp_err_t sntp_client_init(config_device_t *config_device) {

	ESP_LOGI(TAG, "Initializing SNTP");
	config_sys_ptr = &config_device->sys;
	// SNTP service uses LwIP, please allocate large stack space.
	BaseType_t answer = xTaskCreate(sntp_client_task, "sntp_client_task", 2048, NULL, 10, NULL);
	if (answer != pdPASS) {
		ESP_LOGE(TAG, "Error xTaskCreate: sntp_client_task");
	}
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void sntp_client_task(void *arg) {
	time_t now = { 0 };
	struct tm timeinfo = { 0 };
	char strftime_buf[64];

//	setenv("TZ", "MSK-3", 1);
	setenv("TZ", config_sys_ptr->timezone, 1);
	tzset();

	while (1) {
		// update 'now' variable with current time
		time(&now);
		localtime_r(&now, &timeinfo);

		if (timeinfo.tm_year < (2016 - 1900)) {
			ESP_LOGE(TAG, "The current date/time error");
			sntp_client_obtain_time();
		} else {
			strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
			ESP_LOGI(TAG, "The current date/time: %s", strftime_buf);
		}
//		ESP_LOGI(TAG, "Free heap size: %d\n", esp_get_free_heap_size());
		vTaskDelay((60000) / portTICK_RATE_MS);
	}
}
//-----------------------------------------------------------------------------------------------
void sntp_client_initialize(void) {
	sntp_setoperatingmode(SNTP_OPMODE_POLL);
	sntp_setservername(0, config_sys_ptr->sntp_server);
//	sntp_set_timezone(3);
	sntp_init();
}
//-----------------------------------------------------------------------------------------------
void sntp_client_obtain_time(void) {
	xEventGroupWaitBits(wifi_event_group, WIFI_CONNECTED_BIT, false, true, portMAX_DELAY);
	sntp_client_initialize();

	// wait for time to be set
	time_t now = 0;
	struct tm timeinfo = { 0 };
	char strftime_buf[64];
	int retry = 0;
	const int retry_count = 10;

	while (timeinfo.tm_year < (2016 - 1900) && ++retry < retry_count) {
		ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
		vTaskDelay(2000 / portTICK_PERIOD_MS);
		time(&now);
		localtime_r(&now, &timeinfo);
	}
	if (retry == retry_count) {
		ESP_LOGI(TAG, "Obtain time, Error");
	} else {
		ESP_LOGI(TAG, "Obtain time, Ok");
		strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
		ESP_LOGI(TAG, "The current date/time: %s", strftime_buf);
	}
}
//-----------------------------------------------------------------------------------------------

