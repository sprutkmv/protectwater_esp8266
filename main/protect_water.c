//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "driver/gpio.h"
#include "esp_log.h"

#include "protect_water.h"
#include "device_mqtt.h"
//-----------------------------------------------------------------------------------------------
#define PW_MQTT_SUBSCRIBE_MAIN 		"/pw/tap/main"
#define PW_MQTT_PUBLISHED_STATUS 	"/pw/tap/main/status"
//-----------------------------------------------------------------------------------------------
static const char *TAG = "protect_water";
static xQueueHandle pw_mqtt_rx_queue = NULL;
const char *state_tap_str[] = { "CLOSE", "OPEN", "CLOSES", "OPENS" };
//-----------------------------------------------------------------------------------------------

static void protect_water_callback_evt_sensor(int num);
static void protect_water_callback_evt_tap_status(state_tap_t state);
void protect_water_send_tap_status(state_tap_t state);
void protect_water_scheduler(char *data);
void protect_water_task();
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
esp_err_t protect_water_init() {
	BaseType_t answer;
	answer = xTaskCreate(protect_water_task, "protect_water_task", 2048, NULL, 2, NULL);
	if (answer != pdPASS) {
		ESP_LOGE(TAG, "Error xTaskCreate: protect_water_task");
		return (ESP_FAIL);
	}
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
void protect_water_scheduler(char *data) {
	if (strcmp(data, "open") == 0) {
		protect_water_tap_open();
	} else if (strcmp(data, "close") == 0) {
		protect_water_tap_close();
	} else if (strcmp(data, "status") == 0) {
		protect_water_send_tap_status(protect_water_tap_get_status());
	}
}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void protect_water_callback_evt_sensor(int num) {
	ESP_LOGI(TAG, "sensor[%d], state: Alarm", num);
	protect_water_tap_close();
}
//-----------------------------------------------------------------------------------------------
static void protect_water_callback_evt_tap_status(state_tap_t state) {
	protect_water_send_tap_status(state);
}
//-----------------------------------------------------------------------------------------------
void protect_water_send_tap_status(state_tap_t state) {
	mqtt_packet_t mqtt_packet;
	ESP_LOGI(TAG, "tap, state: [%d]", state);

	strlcpy(mqtt_packet.topic, PW_MQTT_PUBLISHED_STATUS, sizeof(mqtt_packet.topic));
	strlcpy(mqtt_packet.data, state_tap_str[state], sizeof(mqtt_packet.data));
//	sprintf(mqtt_packet.data, "%d", state);
	device_mqtt_published(&mqtt_packet);
}
//-----------------------------------------------------------------------------------------------
void protect_water_task() {

	protect_water_tap_init();
	protect_water_tap_status_set_evt(protect_water_callback_evt_tap_status);
	protect_water_sensor_init();
	protect_water_sensor_set_evt(protect_water_callback_evt_sensor);

	pw_mqtt_rx_queue = xQueueCreate(10, sizeof(mqtt_packet_data_t));
	if (pw_mqtt_rx_queue == NULL) {
		ESP_LOGE(TAG, "Error xQueueCreate: pw_mqtt_rx_queue");
	}

	device_mqtt_subscribe(PW_MQTT_SUBSCRIBE_MAIN, pw_mqtt_rx_queue);

	mqtt_packet_data_t packet_data;
	for (;;) {
		if (xQueueReceive(pw_mqtt_rx_queue, &packet_data, portMAX_DELAY)) {
			ESP_LOGI(TAG, "pw_mqtt_packet_rx: data= %s", packet_data.data);
			protect_water_scheduler(packet_data.data);
		}
	}
}
//-----------------------------------------------------------------------------------------------
