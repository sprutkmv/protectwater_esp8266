/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : andreyshavlikov@gmail.com
 * License public domain/CC0
 */
#ifndef __WEB_SERVER_H
#define __WEB_SERVER_H
//-----------------------------------------------------------------------------------------------
#include "esp_vfs.h"
#include "config_device.h"
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------

/* Max size of an individual file. Make sure this
 * value is same as that set in upload_script.html */
#define MAX_FILE_SIZE   		(1024*1024) // 200 KB
#define MAX_FILE_SIZE_STR 		"1M"


#define FILE_PATH_MAX (ESP_VFS_PATH_MAX + 128)
#define SCRATCH_BUFSIZE (10240)

#define CHECK_FILE_EXTENSION(filename, ext) (strcasecmp(&filename[strlen(filename) - strlen(ext)], ext) == 0)
//-----------------------------------------------------------------------------------------------
typedef struct web_server_context {
	char base_path[ESP_VFS_PATH_MAX + 1];
	char scratch[SCRATCH_BUFSIZE];
	config_device_t *config_device;
} web_server_context_t;

//-----------------------------------------------------------------------------------------------

esp_err_t web_server_init(const char *base_path, config_device_t *config_device);
//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* __WEB_SERVER_H */
//-----------------------------------------------------------------------------------------------
