//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "freertos/timers.h"

#include "esp_log.h"
#include "esp_system.h"

#include "rom/ets_sys.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"

#include "network_wifi.h"
//-----------------------------------------------------------------------------------------------
#define CONFIG_TIME_RECONNECT_WIFI_MS		(1*60*1000)
#define CONFIG_MAX_COUNT_RECONNECT_STA		10
//-----------------------------------------------------------------------------------------------
static const char *TAG = "network_wifi";
static config_wifi_t *config_wifi_ptr;
static uint32_t count_connected_ap; 	// ���������� ������������ �������� � ����� ������� (� ������ AP)
static uint32_t count_reconnect_sta;		// ���������� ������� ��������������� � ����� �������
wifi_config_t wifi_config;
TimerHandle_t timer_reconnect = NULL;
EventGroupHandle_t wifi_event_group;/* FreeRTOS event group to signal when we are connected*/
//-----------------------------------------------------------------------------------------------
static esp_err_t network_wifi_event_handler(void *ctx, system_event_t *event);
void network_wifi_init_softap();
void network_wifi_init_sta();
static void network_wifi_timer_reconnect();
//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
esp_err_t network_wifi_init(config_device_t *config_device) {

	ESP_LOGI(TAG, "Initializing WiFI");

	config_wifi_ptr = &config_device->wifi;

	esp_err_t ret = nvs_flash_init();		//Initialize NVS
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	wifi_event_group = xEventGroupCreate();
	tcpip_adapter_init();
	ESP_ERROR_CHECK(esp_event_loop_init(network_wifi_event_handler, NULL));
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT()
	;
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

	timer_reconnect = xTimerCreate("tReconn", CONFIG_TIME_RECONNECT_WIFI_MS, pdFALSE, (void*) 0, network_wifi_timer_reconnect);
	if (timer_reconnect == NULL) {
		ESP_LOGE(TAG, "Timer Reconnect, Error");
	}

	memset(&wifi_config, 0, sizeof(wifi_config));

	count_connected_ap = 0;
	count_reconnect_sta = 0;

	if (config_wifi_ptr->wifi_mode == WIFI_MODE_AP) {
		network_wifi_init_softap();
	} else {
		network_wifi_init_sta();
	}
	return (ESP_OK);
}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
static void network_wifi_timer_reconnect() {
	ESP_LOGI(TAG, "Timer Reconnect, Alarm");
	count_reconnect_sta=0;
	network_wifi_init_sta();
}
//-----------------------------------------------------------------------------------------------
static esp_err_t network_wifi_event_handler(void *ctx, system_event_t *event) {
	/* For accessing reason codes in case of disconnection */
	system_event_info_t *info = &event->event_info;

	switch (event->event_id) {
	case SYSTEM_EVENT_STA_START:
		esp_wifi_connect();
		break;
	case SYSTEM_EVENT_AP_START:
		ESP_LOGI(TAG, "SYSTEM_EVENT_AP_START");
		if (config_wifi_ptr->wifi_mode != WIFI_MODE_AP) {
			if (xTimerReset(timer_reconnect, 0) != pdPASS) {
				ESP_LOGI(TAG, "Timer Reconnect Start, Error");
			}
		}
		break;
	case SYSTEM_EVENT_AP_STOP:
		ESP_LOGI(TAG, "SYSTEM_EVENT_AP_STOP");
		break;
	case SYSTEM_EVENT_STA_GOT_IP:
		count_reconnect_sta = 0;
		ESP_LOGI(TAG, "got ip:%s", ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
		xEventGroupSetBits(wifi_event_group, WIFI_CONNECTED_BIT);
		break;
	case SYSTEM_EVENT_AP_STACONNECTED:
		count_connected_ap++;
		ESP_LOGI(TAG, "CONNECTED station:"MACSTR" join, AID=%d  connected [%d]", MAC2STR(event->event_info.sta_connected.mac), event->event_info.sta_connected.aid, count_connected_ap);
		if (config_wifi_ptr->wifi_mode != WIFI_MODE_AP) {
			if (xTimerStop(timer_reconnect, 0) != pdPASS) {
				ESP_LOGI(TAG, "Timer Reconnect Stop, Error");
			}
		}
		break;
	case SYSTEM_EVENT_AP_STADISCONNECTED:
		count_connected_ap--;
		ESP_LOGI(TAG, "DISCONNECTED station:"MACSTR"leave, AID=%d  connected [%d]", MAC2STR(event->event_info.sta_disconnected.mac), event->event_info.sta_disconnected.aid, count_connected_ap);
		if ((config_wifi_ptr->wifi_mode != WIFI_MODE_AP) && (count_connected_ap == 0)) {
			if (xTimerReset(timer_reconnect, 0) != pdPASS) {
				ESP_LOGI(TAG, "Timer Reconnect Restart, Error");
			}
		}
		break;
	case SYSTEM_EVENT_STA_DISCONNECTED:
		ESP_LOGE(TAG, "Disconnect reason : %d", info->disconnected.reason);
		if (info->disconnected.reason == WIFI_REASON_BASIC_RATE_NOT_SUPPORT) {
			esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCAL_11B | WIFI_PROTOCAL_11G | WIFI_PROTOCAL_11N);/*Switch to 802.11 bgn mode */
		}
		count_reconnect_sta++;
		if (count_reconnect_sta > CONFIG_MAX_COUNT_RECONNECT_STA) {
			network_wifi_init_softap();
		} else {
			esp_wifi_connect();
		}
		xEventGroupClearBits(wifi_event_group, WIFI_CONNECTED_BIT);
		break;
	default:
		break;
	}
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
void network_wifi_init_softap() {

	ESP_LOGI(TAG, "ESP_WIFI_MODE_AP");
	strlcpy((char*)wifi_config.ap.ssid, config_wifi_ptr->ssid_ap, sizeof(wifi_config.ap.ssid));
	wifi_config.ap.ssid_len = strlen((char*)wifi_config.ap.ssid);
	strlcpy((char*)wifi_config.ap.password, config_wifi_ptr->password_ap, sizeof(wifi_config.ap.password));
	wifi_config.ap.max_connection = config_wifi_ptr->max_connection;
	wifi_config.ap.authmode = config_wifi_ptr->authmode;

	if (strlen((char*)wifi_config.ap.password) < 8) {
		wifi_config.ap.authmode = WIFI_AUTH_OPEN;
	}

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());

	ESP_LOGI(TAG, "Wifi_init_softap finished.SSID:%s password:%s", wifi_config.ap.ssid, wifi_config.ap.password);
}
//-----------------------------------------------------------------------------------------------
void network_wifi_init_sta() {
	ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
	strlcpy((char*)wifi_config.sta.ssid, config_wifi_ptr->ssid, sizeof(wifi_config.sta.ssid));
	strlcpy((char*)wifi_config.sta.password, config_wifi_ptr->password, sizeof(wifi_config.sta.password));

	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
	ESP_ERROR_CHECK(esp_wifi_start());

	ESP_LOGI(TAG, "wifi_init_sta finished.");
	ESP_LOGI(TAG, "connect to ap SSID:%s password:%s", wifi_config.sta.ssid, wifi_config.sta.password);
}
//-----------------------------------------------------------------------------------------------

