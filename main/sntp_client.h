/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : ghetman@gmail.com
 * License public domain/CC0
 */
#ifndef __SNTP_CLIENT_H
#define __SNTP_CLIENT_H
//-----------------------------------------------------------------------------------------------
#include "config_device.h"
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------
esp_err_t sntp_client_init(config_device_t *config_device);





//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* __SNTP_CLIENT_H */
//-----------------------------------------------------------------------------------------------
