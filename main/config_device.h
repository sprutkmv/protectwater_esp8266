/*
 * andreyshavlikov@gmail.com
 * Author  Andrey Shavlikov : andreyshavlikov@gmail.com
 * License public domain/CC0
 */
#ifndef __CONFIG_DEVICE_H
#define __CONFIG_DEVICE_H
//-----------------------------------------------------------------------------------------------
#include "esp_wifi.h"
//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
extern "C" {
#endif
//-----------------------------------------------------------------------------------------------
//#define CONFIG_ESP_FILE_CONFIG_WIFI		"config_wifi.json"
//#define CONFIG_ESP_FILE_CONFIG_SYS		"config_sys.json"
//#define CONFIG_ESP_FILE_CONFIG_MQTT		"config_mqtt.json"
//-----------------------------------------------------------------------------------------------
typedef struct {
	char uri[32]; /*!< Complete MQTT broker URI */
	uint32_t port; /*!< MQTT server port */
	char username[32]; /*!< MQTT username */
	char password[64]; /*!< MQTT password */

} config_mqtt_t;

typedef struct {
	char ssid[32]; /**< SSID of ESP8266 soft-AP */
	char password[64]; /**< Password of ESP8266 soft-AP */
	wifi_auth_mode_t authmode; /**< Auth mode of ESP8266 soft-AP. Do not support AUTH_WEP in soft-AP mode */
	wifi_mode_t wifi_mode; // STA or AP
	uint8_t max_connection; /**< Max number of stations allowed to connect in, default 4, max 4 */

	char ssid_ap[32]; /**< SSID of ESP8266 soft-AP */
	char password_ap[64]; /**< Password of ESP8266 soft-AP */
} config_wifi_t;

typedef struct {
	char host_name[32];
	char timezone[16];
	char sntp_server[32];
} config_sys_t;

typedef struct {
	config_wifi_t wifi;
	config_sys_t sys;
	config_mqtt_t mqtt;
} config_device_t;




//-----------------------------------------------------------------------------------------------
esp_err_t config_device_init(const char *base_path);
config_device_t* config_device_get_ptr();
esp_err_t config_device_save_file(config_device_t *config_device);
esp_err_t config_device_load_file(config_device_t *config_device);
//esp_err_t config_device_parser_json_str(config_device_t *config_device, char *jbuff);
//esp_err_t config_device_create_json_str(config_device_t *config_device, char *jbuff, uint32_t size);

esp_err_t config_wifi_create_json_str(config_wifi_t * config_wifi, char *jbuff, uint32_t size);
esp_err_t config_sys_create_json_str(config_sys_t *config_sys, char *jbuff, uint32_t size);
esp_err_t config_mqtt_create_json_str(config_mqtt_t *config_mqtt, char *jbuff, uint32_t size);

esp_err_t config_wifi_parser_json_str(config_wifi_t *config_wifi, char *jbuff);
esp_err_t config_sys_parser_json_str(config_sys_t *config_sys, char *jbuff);
esp_err_t config_mqtt_parser_json_str(config_mqtt_t *config_mqtt, char *jbuff);


esp_err_t config_wifi_save_file(config_wifi_t *config_wifi);
esp_err_t config_sys_save_file(config_sys_t *config_sys);
esp_err_t config_mqtt_save_file(config_mqtt_t *config_mqtt);

esp_err_t config_wifi_load_file(config_wifi_t *config_wifi);
esp_err_t config_sys_load_file(config_sys_t *config_sys);
esp_err_t config_mqtt_load_file(config_mqtt_t *config_mqtt);

//-----------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------
#ifdef	__cplusplus
}
#endif

#endif  /* __CONFIG_DEVICE_H */
//-----------------------------------------------------------------------------------------------
