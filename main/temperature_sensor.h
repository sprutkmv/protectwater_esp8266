#ifndef DS18B20_ONEWIRE_H_
#define DS18B20_ONEWIRE_H_

#include "ds18b20.h"

#ifdef	__cplusplus
extern "C" {
#endif

/* ds18b20_onewire.c - Retrieves readings from one or more DS18B20 temperature
 * sensors, and prints the results to stdout.
 *
 * This sample code is in the public domain.,
 */


void temperature_sensor_init(void);











#ifdef	__cplusplus
}
#endif

#endif  /* DS18B20_ONEWIRE_H_ */
