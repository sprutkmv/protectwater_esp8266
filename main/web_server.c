#include <string.h>
#include <sys/param.h>
#include <fcntl.h>
#include "esp_http_server.h"
#include "esp_system.h"
#include "esp_log.h"
#include "cJSON.h"
#include "esp_ota_ops.h"
#include "nvs.h"
#include "nvs_flash.h"

#include "web_server.h"
//-----------------------------------------------------------------------------------------------
#define CONFIG_ESP_GET_URI_COMMON			"/*"
#define CONFIG_ESP_GET_URI_CONFIG			"/configs.json"
#define CONFIG_ESP_GET_URI_CONFIG_WIFI		"/configs_wifi.json"
#define CONFIG_ESP_GET_URI_CONFIG_SYS		"/configs_sys.json"
#define CONFIG_ESP_GET_URI_CONFIG_MQTT		"/configs_mqtt.json"

#define CONFIG_ESP_GET_URI_UPLOAD			"/upload/*"
//-----------------------------------------------------------------------------------------------
static const char *TAG = "web-server";
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_set_content_type_from_file(httpd_req_t *req, const char *filepath);
static esp_err_t web_server_common_get_handler(httpd_req_t *req);
static esp_err_t web_server_config_get_handler(httpd_req_t *req);
static esp_err_t web_server_config_wifi_get_handler(httpd_req_t *req);
static esp_err_t web_server_config_sys_get_handler(httpd_req_t *req);
static esp_err_t web_server_config_mqtt_get_handler(httpd_req_t *req);

static esp_err_t web_server_upload_post_handler(httpd_req_t *req);
//static esp_err_t web_server_ota_next_handler(httpd_req_t *req);
static const char* web_server_get_path_from_uri(char *dest, const char *base_path, const char *uri, size_t destsize);

//-----------------------------------------------------------------------------------------------
// Public
//-----------------------------------------------------------------------------------------------
esp_err_t web_server_init(const char *base_path, config_device_t *config_device) {

	ESP_LOGI(TAG, "Initializing Web Server");
	if (base_path == NULL) {
		ESP_LOGE(TAG, "Base path? Error");
		return (ESP_FAIL);
	}

	if (config_device == NULL) {
		ESP_LOGE(TAG, "Config_device, Error");
		return (ESP_FAIL);
	}

	web_server_context_t *web_context = calloc(1, sizeof(web_server_context_t));
	if (web_context == NULL) {
		ESP_LOGE(TAG, "No memory for rest context");
		return (ESP_FAIL);
	}
	strlcpy(web_context->base_path, base_path, sizeof(web_context->base_path));
	web_context->config_device = config_device;
	httpd_handle_t server = NULL;
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();
	config.uri_match_fn = httpd_uri_match_wildcard;

	ESP_LOGI(TAG, "Starting HTTP Server");

	if (httpd_start(&server, &config) != ESP_OK) {
		ESP_LOGE(TAG, "Start server, failed");
		free(web_context);
		return (ESP_FAIL);
	}

	// URI handler for getting web server files

	httpd_uri_t common_get_uri = { .uri = CONFIG_ESP_GET_URI_COMMON, .method = HTTP_GET, .handler = web_server_common_get_handler, .user_ctx =
			web_context };
	httpd_register_uri_handler(server, &common_get_uri);

	httpd_uri_t config_get_uri = { .uri = CONFIG_ESP_GET_URI_CONFIG, .method = HTTP_PUT, .handler = web_server_config_get_handler, .user_ctx =
			web_context };
	httpd_register_uri_handler(server, &config_get_uri);

	httpd_uri_t config_wifi_get_uri = { .uri = CONFIG_ESP_GET_URI_CONFIG_WIFI, .method = HTTP_PUT, .handler = web_server_config_wifi_get_handler,
			.user_ctx = web_context };
	httpd_register_uri_handler(server, &config_wifi_get_uri);

	httpd_uri_t config_sys_get_uri = { .uri = CONFIG_ESP_GET_URI_CONFIG_SYS, .method = HTTP_PUT, .handler = web_server_config_sys_get_handler,
			.user_ctx = web_context };
	httpd_register_uri_handler(server, &config_sys_get_uri);

	httpd_uri_t config_mqtt_get_uri = { .uri = CONFIG_ESP_GET_URI_CONFIG_MQTT, .method = HTTP_PUT, .handler = web_server_config_mqtt_get_handler,
			.user_ctx = web_context };
	httpd_register_uri_handler(server, &config_mqtt_get_uri);

	httpd_uri_t file_upload = { .uri = CONFIG_ESP_GET_URI_UPLOAD, .method = HTTP_POST, .handler = web_server_upload_post_handler, .user_ctx =
			web_context };
	httpd_register_uri_handler(server, &file_upload);

	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
// Private
//-----------------------------------------------------------------------------------------------
// Send HTTP response with the contents of the requested file
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_common_get_handler(httpd_req_t *req) {
	char filepath[FILE_PATH_MAX];
	char filepath_withgz[FILE_PATH_MAX];
	int fd;
	struct stat st;
	web_server_context_t *web_context = (web_server_context_t*) req->user_ctx;

	strlcpy(filepath, web_context->base_path, sizeof(filepath));

	if (req->uri[strlen(req->uri) - 1] == '/') {
		strlcat(filepath, "/index.htm", sizeof(filepath));
//		if (stat(filepath, &st) != 0) { // Check if destination file exists
//			strlcat(filepath, "l", sizeof(filepath)); // /index.html
//		}
	} else {
		strlcat(filepath, req->uri, sizeof(filepath));
	}

	web_server_set_content_type_from_file(req, filepath);

	strlcpy(filepath_withgz, filepath, sizeof(filepath_withgz));
	strlcat(filepath_withgz, ".gz", sizeof(filepath_withgz));

	// Check if destination file exists
	if (stat(filepath_withgz, &st) == 0) {
		strlcpy(filepath, filepath_withgz, sizeof(filepath));
		httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
	}

	ESP_LOGI(TAG, "open file : %s", filepath);
	fd = open(filepath, O_RDONLY, 0);
	if (fd == -1) {
		ESP_LOGE(TAG, "Failed to open file : %s", filepath);
		/* Respond with 500 Internal Server Error */
		httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to read existing file");
		return ESP_FAIL;
	}

//	set_content_type_from_file(req, filepath);

	char *chunk = web_context->scratch;
	ssize_t read_bytes;
	do {
		/* Read file in chunks into the scratch buffer */
		read_bytes = read(fd, chunk, SCRATCH_BUFSIZE);
		if (read_bytes == -1) {
			ESP_LOGE(TAG, "Failed to read file : %s", filepath);
		} else if (read_bytes > 0) {
			/* Send the buffer contents as HTTP response chunk */
			if (httpd_resp_send_chunk(req, chunk, read_bytes) != ESP_OK) {
				close(fd);
				ESP_LOGE(TAG, "File sending failed!");
				/* Abort sending file */
				httpd_resp_sendstr_chunk(req, NULL);
				/* Respond with 500 Internal Server Error */
				httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to send file");
				return ESP_FAIL;
			}
		}
	} while (read_bytes > 0);
	/* Close file after sending complete */
	close(fd);
	ESP_LOGI(TAG, "File sending complete");
	/* Respond with an empty chunk to signal HTTP response completion */
	httpd_resp_send_chunk(req, NULL, 0);
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_config_get_handler(httpd_req_t *req) {
	time_t now;
	struct tm timeinfo;
	char strftime_buf[64];
	web_server_context_t *web_context = (web_server_context_t*) req->user_ctx;

	httpd_resp_set_type(req, "application/json");
	cJSON *root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "SSDP", web_context->config_device->sys.host_name);
	cJSON_AddStringToObject(root, "ssidAP", web_context->config_device->wifi.ssid_ap);
	cJSON_AddStringToObject(root, "passwordAP", web_context->config_device->wifi.password_ap);
	cJSON_AddStringToObject(root, "ssid", web_context->config_device->wifi.ssid);
	cJSON_AddStringToObject(root, "password", web_context->config_device->wifi.password);
	cJSON_AddStringToObject(root, "timezone", web_context->config_device->sys.timezone);

	cJSON_AddStringToObject(root, "ip", "192.168.1.225");

	setenv("TZ", web_context->config_device->sys.timezone, 1);
	tzset();
	time(&now);
	localtime_r(&now, &timeinfo);
	strftime(strftime_buf, sizeof(strftime_buf), "%X", &timeinfo);
	cJSON_AddStringToObject(root, "time", strftime_buf);
	strftime(strftime_buf, sizeof(strftime_buf), "%x", &timeinfo);
	cJSON_AddStringToObject(root, "date", strftime_buf);

//    esp_chip_info_t chip_info;
//    esp_chip_info(&chip_info);
//    cJSON_AddStringToObject(root, "version", IDF_VER);
//    cJSON_AddNumberToObject(root, "cores", chip_info.cores);

//	const char *config_info = cJSON_Print(root);
	cJSON_PrintPreallocated(root, web_context->scratch, sizeof(web_context->scratch), 1);
//	httpd_resp_sendstr(req, config_info);
	httpd_resp_sendstr(req, web_context->scratch);
	ESP_LOGI(TAG, "config_info: %s", web_context->scratch);
//	free((void*) config_info);
	cJSON_Delete(root);
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_config_wifi_get_handler(httpd_req_t *req) {

	web_server_context_t *web_context = (web_server_context_t*) req->user_ctx;
	config_wifi_t *config_wifi = &web_context->config_device->wifi;
	httpd_resp_set_type(req, "application/json");
	config_wifi_create_json_str(config_wifi, web_context->scratch, sizeof(web_context->scratch));
	httpd_resp_sendstr(req, web_context->scratch);
	ESP_LOGI(TAG, "config_wifi_get: %s", web_context->scratch);
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_config_sys_get_handler(httpd_req_t *req) {

	web_server_context_t *web_context = (web_server_context_t*) req->user_ctx;
	config_sys_t *config_sys = &web_context->config_device->sys;
	httpd_resp_set_type(req, "application/json");
	config_sys_create_json_str(config_sys, web_context->scratch, sizeof(web_context->scratch));
	httpd_resp_sendstr(req, web_context->scratch);
	ESP_LOGI(TAG, "config_sys_get: %s", web_context->scratch);
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_config_mqtt_get_handler(httpd_req_t *req) {

	web_server_context_t *web_context = (web_server_context_t*) req->user_ctx;
	config_mqtt_t *config_mqtt = &web_context->config_device->mqtt;
	httpd_resp_set_type(req, "application/json");
	config_mqtt_create_json_str(config_mqtt, web_context->scratch, sizeof(web_context->scratch));
	httpd_resp_sendstr(req, web_context->scratch);
	ESP_LOGI(TAG, "config_mqtt_get: %s", web_context->scratch);
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
// Set HTTP response content type according to file extension
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_set_content_type_from_file(httpd_req_t *req, const char *filepath) {
	const char *type = "text/plain";

	if (CHECK_FILE_EXTENSION(filepath, ".htm")) {
		type = "text/html";
	} else if (CHECK_FILE_EXTENSION(filepath, ".html")) {
		type = "text/html";
	} else if (CHECK_FILE_EXTENSION(filepath, ".json")) {
		type = "application/json";
	} else if (CHECK_FILE_EXTENSION(filepath, ".js")) {
		type = "application/javascript";
	} else if (CHECK_FILE_EXTENSION(filepath, ".css")) {
		type = "text/css";
	} else if (CHECK_FILE_EXTENSION(filepath, ".png")) {
		type = "image/png";
	} else if (CHECK_FILE_EXTENSION(filepath, ".ico")) {
		type = "image/x-icon";
	} else if (CHECK_FILE_EXTENSION(filepath, ".svg")) {
		type = "text/xml";
	} else if (CHECK_FILE_EXTENSION(filepath, ".gif")) {
		type = "image/gif";
	} else if (CHECK_FILE_EXTENSION(filepath, ".jpg")) {
		type = "image/jpeg";
	} else if (CHECK_FILE_EXTENSION(filepath, ".xml")) {
		type = "text/xml";
	} else if (CHECK_FILE_EXTENSION(filepath, ".pdf")) {
		type = "application/x-pdf";
	} else if (CHECK_FILE_EXTENSION(filepath, ".zip")) {
		type = "application/x-zip";
	} else if (CHECK_FILE_EXTENSION(filepath, ".gz")) {
		type = "application/x-gzip";
	}
	return httpd_resp_set_type(req, type);
}
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// Copies the full path into destination buffer and returns
// pointer to path (skipping the preceding base path)
//-----------------------------------------------------------------------------------------------
static const char*
web_server_get_path_from_uri(char *dest, const char *base_path, const char *uri, size_t destsize) {
	const size_t base_pathlen = strlen(base_path);
	size_t pathlen = strlen(uri);

	const char *quest = strchr(uri, '?');
	if (quest) {
		pathlen = MIN(pathlen, quest - uri);
	}
	const char *hash = strchr(uri, '#');
	if (hash) {
		pathlen = MIN(pathlen, hash - uri);
	}

	if (base_pathlen + pathlen + 1 > destsize) {
		/* Full path string won't fit into destination buffer */
		return NULL;
	}
	/* Construct full path (base + path) */
	strcpy(dest, base_path);
	strlcpy(dest + base_pathlen, uri, pathlen + 1);

	/* Return pointer to path, skipping the base */
	return dest + base_pathlen;
}
//-----------------------------------------------------------------------------------------------
static esp_err_t web_server_upload_post_handler(httpd_req_t *req) {
	char filepath[FILE_PATH_MAX];
//	FILE *fd = NULL;
	struct stat file_stat;
	esp_err_t err;
	/* update handle : set by esp_ota_begin(), must be freed via esp_ota_end() */
	esp_ota_handle_t update_handle = 0;
	const esp_partition_t *update_partition = NULL;

	ESP_LOGI(TAG, "upload_post_handler: content_len[%d], uri[%s]", req->content_len, req->uri);

	/* Skip leading "/upload" from URI to get filename */
	/* Note sizeof() counts NULL termination hence the -1 */
	const char *filename = web_server_get_path_from_uri(filepath, ((web_server_context_t*) req->user_ctx)->base_path,
			req->uri + sizeof("/upload") - 1, sizeof(filepath));
	if (!filename) {
		/* Respond with 500 Internal Server Error */
		httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Filename too long");
		return ESP_FAIL;
	}

	/* Filename cannot have a trailing '/' */
	if (filename[strlen(filename) - 1] == '/') {
		ESP_LOGE(TAG, "Invalid filename : %s", filename);
		httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Invalid filename");
		return ESP_FAIL;
	}

	if (stat(filepath, &file_stat) == 0) {
		ESP_LOGE(TAG, "File already exists : %s", filepath);
		/* Respond with 400 Bad Request */
		httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "File already exists");
		return ESP_FAIL;
	}

	/* File cannot be larger than a limit */
	if (req->content_len > MAX_FILE_SIZE) {
		ESP_LOGE(TAG, "File too large : %d bytes", req->content_len);
		/* Respond with 400 Bad Request */
		httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "File size must be less than "
		MAX_FILE_SIZE_STR "!");
		/* Return failure to close underlying connection else the
		 * incoming file content will keep the socket busy */
		return ESP_FAIL;
	}

	ESP_LOGI(TAG, "Starting OTA example... @  flash %s", CONFIG_ESPTOOLPY_FLASHSIZE);

	const esp_partition_t *configured = esp_ota_get_boot_partition();
	const esp_partition_t *running = esp_ota_get_running_partition();

	if (configured != running) {
		ESP_LOGW(TAG, "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x", configured->address, running->address);
		ESP_LOGW(TAG, "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
	}
	ESP_LOGI(TAG, "Running partition type %d subtype %d (offset 0x%08x)", running->type, running->subtype, running->address);

	update_partition = esp_ota_get_next_update_partition(NULL);
	ESP_LOGI(TAG, "Writing to partition subtype %d at offset 0x%x", update_partition->subtype, update_partition->address);
	assert(update_partition != NULL);

	err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "esp_ota_begin failed, error=%d", err);
		return ESP_FAIL;
	}
	ESP_LOGI(TAG, "esp_ota_begin succeeded");

//    fd = fopen(filepath, "w");
//    if (!fd) {
//        ESP_LOGE(TAG, "Failed to create file : %s", filepath);
//        /* Respond with 500 Internal Server Error */
//        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to create file");
//        return ESP_FAIL;
//    }
	ESP_LOGI(TAG, "filepath: %s...", filepath);
	ESP_LOGI(TAG, "Receiving file : %s...", filename);

	/* Retrieve the pointer to scratch buffer for temporary storage */
	char *buf = ((web_server_context_t*) req->user_ctx)->scratch;
	int received;

	/* Content length of the request gives
	 * the size of the file being uploaded */
	int remaining = req->content_len;

	while (remaining > 0) {

		/* Receive the file part by part into a buffer */
		if ((received = httpd_req_recv(req, buf, MIN(remaining, SCRATCH_BUFSIZE))) <= 0) {
			if (received == HTTPD_SOCK_ERR_TIMEOUT) {
				/* Retry if timeout occurred */
				continue;
			}

			/* In case of unrecoverable error,
			 * close and delete the unfinished file*/
//            fclose(fd);
//            unlink(filepath);
			ESP_LOGE(TAG, "File reception failed!");
			/* Respond with 500 Internal Server Error */
			httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to receive file");
			return ESP_FAIL;
		}
		ESP_LOGI(TAG, "Remaining size : %d, received: %d", remaining, received);

		err = esp_ota_write(update_handle, (const void*) buf, received);
		if (err != ESP_OK) {
			ESP_LOGE(TAG, "Error: esp_ota_write failed! err=0x%x", err);
			return ESP_FAIL;
		}

		/* Write buffer content to file on storage */
//        if (received && (received != fwrite(buf, 1, received, fd))) {
//            /* Couldn't write everything to file!
//             * Storage may be full? */
//            fclose(fd);
//            unlink(filepath);
//
//            ESP_LOGE(TAG, "File write failed!");
//            /* Respond with 500 Internal Server Error */
//            httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to write file to storage");
//            return ESP_FAIL;
//        }
		/* Keep track of remaining size of
		 * the file left to be uploaded */
		remaining -= received;
	}

	/* Close file upon upload completion */
//    fclose(fd);
	ESP_LOGI(TAG, "File reception complete");

	/* Redirect onto root to see the updated file list */
	httpd_resp_set_status(req, "303 See Other");
	httpd_resp_set_hdr(req, "Location", "/");
	httpd_resp_sendstr(req, "File uploaded successfully");

	if (esp_ota_end(update_handle) != ESP_OK) {
		ESP_LOGE(TAG, "esp_ota_end failed!");
		return ESP_FAIL;
	}
	err = esp_ota_set_boot_partition(update_partition);
	if (err != ESP_OK) {
		ESP_LOGE(TAG, "esp_ota_set_boot_partition failed! err=0x%x", err);
		return ESP_FAIL;
	}
	ESP_LOGI(TAG, "Prepare to restart system!");
	esp_restart();
	return ESP_OK;
}
//-----------------------------------------------------------------------------------------------
